#include "pch.h"
#include "Block.h"

void Block::InitSprite(const Position& pos, sf::Texture* texture, sf::IntRect rect)
{
	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*texture);
	m_sprite->setTextureRect(rect);
	m_sprite->setScale(0.4f, 0.4f);
	SetSpritePosition(pos);
}

void Block::SetSpritePosition(Position pos)
{
	sf::Vector2f scale = m_sprite->getScale();
	int size = m_sprite->getTextureRect().width;
	m_sprite->setPosition(pos.first*scale.x*size, pos.second*scale.y*size);
}


Block::Block(const Position& pos, sf::Texture* texture, sf::IntRect* rect) : m_position(pos)
{
	if (texture != nullptr) InitSprite(pos, texture, *rect);
	else m_sprite = nullptr;
}

void Block::Render(sf::RenderTarget& target)
{
	if (this == NULL)
		return;
	if (m_sprite)
	{
		target.draw(*m_sprite);
	}

}

void Block::Update(const float& dt)
{
}

Block::~Block()
{
	delete m_sprite;
	

}

char Block::GetSymbol() const
{
	return m_symbol;
}

char Block::GetColor() const
{
	return m_color;
}

void Block::SetSymbol(const char symbol)
{
	m_symbol = symbol;
}

void Block::SetColor(const unsigned char color)
{
	m_color = color;
}

Block::Position Block::GetPosition() const
{
	return m_position;
}

void Block::SetPosition(const Position& otherPos)
{	
	if(m_sprite) SetSpritePosition(otherPos);
	m_position = otherPos;
}

std::ostream & operator<<(std::ostream & in, const Block & block)
{
	// TODO: insert return statement here
	COORD coord;
	coord.X = block.m_position.first;
	coord.Y = block.m_position.second;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE),
		coord
	);

	CONSOLE_CURSOR_INFO     cursorInfo;
	GetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
	cursorInfo.bVisible = false; // set the cursor visibility
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdout, block.m_color);
	in << block.GetSymbol();
	SetConsoleTextAttribute(hStdout, 1 | 2|4);
	return in;
}
