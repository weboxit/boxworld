#pragma once
#include "MutableBlock.h"
class Box :
	public MutableBlock
{
public:	
	Box(const Position& pos, sf::Texture* texture=nullptr, sf::IntRect* rect = nullptr);
	~Box();
	void ChangeState(bool isOverTarget);
	void Update(const float& dt);
	void Render(sf::RenderTarget& target);
};

