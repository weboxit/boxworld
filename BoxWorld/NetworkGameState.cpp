#include "pch.h"
#include "NetworkGameState.h"

NetworkGameState::NetworkGameState(std::shared_ptr < sf::RenderWindow >& window,
	const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
	const std::shared_ptr < GraphicsSettings >& gfxSettings,
	std::shared_ptr < std::stack < State* > >& states,
	const std::shared_ptr<sf::Font>& font, sf::Texture* menu_texture)

	: State(window, suportedKeys, gfxSettings, states),
	m_font(font),
	m_menuTexture(menu_texture)
{
	m_networkMode = NetworkMode::SELECT_MODE;
	m_buttonId = 0;
	InitSelectModeButtons();
	InitKeysUsage();
	InitKeyBindings();
	InitTextures();
	InitLevelsFile();
	InitTextField(); 
	InitServerButtons();
	InitEnterIpText();
	InitBackground(m_menuTexture);
}

NetworkGameState::~NetworkGameState()
{
	delete m_map;
}

void NetworkGameState::HandleKeyboardInput(const sf::Event& current_event)
{
	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Up"])) m_mp.MoveUp();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Down"])) m_mp.MoveDown();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Right"])) m_mp.MoveRight();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Left"])) m_mp.MoveLeft();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Undo"])) m_mp.Undo();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Restart_level"])) m_mp.RestartLevel();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Next_level"])) m_mp.NextLevel();

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Close"]))
	{
		if (m_networkMode == NetworkMode::SELECT_MODE || m_networkMode == NetworkMode::GAME)
			SetQuit();
		else if (m_networkMode == NetworkMode::CLIENT || m_networkMode == NetworkMode::SERVER)
		{
			if (m_networkMode == NetworkMode::CLIENT)
			{
				m_stringInput.clear();
			}
				m_networkMode = NetworkMode::SELECT_MODE;
				m_buttonId = 0;
		}
	}

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Enter"]))
	{
		if (m_networkMode == NetworkMode::CLIENT)
		{
			std::cout << m_stringInput;
			if (m_mp.Connect(m_stringInput,9876))
			{
				m_networkMode = NetworkMode::GAME;
				InitMultiplayer();
				InitBackground(m_textures["game_background"]);
			}
			else {
				m_stringInput.clear();
			}
		}
		else if(UpdateActiveButtons(current_event)) HandleButtons();
	}


	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Select_Up"]))
	{
			if (m_buttonId == 0) m_buttonId++;
			else if (m_buttonId > 1) m_buttonId--;
	}

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Select_Down"]))
	{
		if (m_buttonId == 0) m_buttonId += 2;

		else if(m_networkMode==NetworkMode::SELECT_MODE)
		{
			if (m_buttonId == 0) m_buttonId += 2;
			else if (m_buttonId >= 1 && m_buttonId < m_selectModeButtons.size()) m_buttonId++;
			else if (m_buttonId == m_selectModeButtons.size()) m_buttonId = 1;
		}

		else if (m_networkMode == NetworkMode::SERVER)
		{
			if (m_buttonId == 0) m_buttonId += 2;
			else if (m_buttonId >= 1 && m_buttonId < m_serverButtons.size()) m_buttonId++;
			else if (m_buttonId == m_serverButtons.size()) m_buttonId = 1;
		}
	}
}

bool NetworkGameState::UpdateActiveButtons(const sf::Event&)
{
	if (m_networkMode == NetworkMode::SELECT_MODE)
	{
		for (auto& it : m_selectModeButtons)
		{
			if (it.second.UpdateActiveState()) 	return true;
		}	
		return false;
	}
	else if (m_networkMode == NetworkMode::SERVER)
	{
		for (auto& it : m_serverButtons)
		{
			if (it.second.UpdateActiveState()) 	return true;
		}
		return false;
	}
	else return false;
}

void NetworkGameState::HandleButtons()
{
	if (m_networkMode == NetworkMode::SELECT_MODE)
	{
		if (m_selectModeButtons["JOIN_SERVER"].IsPressed())
		{
			m_networkMode = NetworkMode::CLIENT;
			/*std::cin >> m_stringInput;*/
		}
		else if (m_selectModeButtons["CREATE_SERVER"].IsPressed())
		{
			m_networkMode = NetworkMode::SERVER;
			m_buttonId = 0;
			
		}
	}
	else if (m_networkMode == NetworkMode::SERVER)
	{
		int number_of_players = 0;

		for (auto& it : m_serverButtons)
		{
			if (it.second.IsPressed()) {
				number_of_players = it.second.GetId() + 1;
				if (number_of_players == 2) m_mp.SetFile("./Resources/Maps/multyPlayerLevels.txt");
				if (number_of_players == 3) m_mp.SetFile("./Resources/Maps/3playeri.txt");
				if (number_of_players == 4) m_mp.SetFile("./Resources/Maps/4playeri.txt");
			}
		}

		if (number_of_players != 0)
		{
			m_networkMode = NetworkMode::GAME;
			InitBackground(m_textures["game_background"]);
			InitMultiplayer();
			m_mp.Server(number_of_players);
		}
	}
}

void NetworkGameState::HandleSFMLEvents(const sf::Event& current_event)
{
	if (current_event.type == sf::Event::TextEntered)
	{
		if (current_event.text.unicode == '\b'
			&& m_stringInput.size()) m_stringInput.erase(m_stringInput.size() - 1, 1);
		else if (current_event.text.unicode < 128)
			m_stringInput += static_cast<char>(current_event.text.unicode);

		m_ipEntered.setString(m_stringInput);
	}

	else if (current_event.type == sf::Event::MouseButtonPressed &&
		current_event.mouseButton.button == sf::Mouse::Button::Left &&
		UpdateActiveButtons(current_event))
	{
		HandleButtons();
	}

	else if (current_event.type == sf::Event::KeyPressed) HandleKeyboardInput(current_event);
}

void NetworkGameState::Update(const float& dt)
{
	UpdateMousePositions();
	UpdateButtonsHover();
	m_map->UpdatePlayers(dt);
	if (m_networkMode == NetworkMode::GAME)
		if (Target::NoTargetsLeft()) m_levelsFin >> m_mp;
}

void NetworkGameState::RenderSelectButtons()
{
	for (auto& it : m_selectModeButtons)
	{
		it.second.Render(*m_window);
	}
}

void NetworkGameState::RenderNumberButtons()
{
	for (auto& it : m_serverButtons)
	{
		it.second.Render(*m_window);
	}
}

inline void NetworkGameState::InitLevelsFile()
{
	m_levelsFin = std::ifstream("./Resources/Maps/multyPlayerLevels.txt");
}

void NetworkGameState::UpdateButtonsHover()
{
	if (m_networkMode == NetworkMode::SELECT_MODE)
	{
		for (auto& it : m_selectModeButtons)
		{
			it.second.UpdateHoverState(m_mousePosWindow, m_buttonId);
		}
	}
	else if (m_networkMode == NetworkMode::SERVER)
	{
		for (auto& it : m_serverButtons)
		{
			it.second.UpdateHoverState(m_mousePosWindow, m_buttonId);
		}
	}
}

void NetworkGameState::InitKeyBindings()
{
	std::ifstream fin("./Config/network_state-keybindings.ini");

	if (fin.is_open())
	{
		std::string first_key = "";
		std::string second_key = "";
		while (fin >> first_key >> second_key)
		{
			if (m_suportedKeys->find(second_key) != m_suportedKeys->end())
				m_keyBindings[first_key] = m_suportedKeys->at(second_key);
		}
	}
	fin.close();
}

void NetworkGameState::InitTextures()
{
	sf::Texture* texture = new sf::Texture;
	texture->loadFromFile("./Resources/Textures/boxworld_texture.jpeg");
	m_textures["boxworld_texture"] = texture;

	texture = new sf::Texture;
	texture->loadFromFile("./Resources/Backgrounds/game_background.jpg");
	m_textures["game_background"] = texture;
}

inline void NetworkGameState::InitMultiplayer()
{
	delete m_map;
	m_map = new Map(m_textures["boxworld_texture"]);
	m_mp.Setmap(m_map);
}

inline void NetworkGameState::InitBackground(sf::Texture* texture)
{
	m_background = sf::RectangleShape();
	m_background.setSize(
		sf::Vector2f
		(
			static_cast<float>(m_gfxSettings->GetResolution().width),
			static_cast<float>(m_gfxSettings->GetResolution().height)
		)
	);
	m_background.setTexture(texture);

}

void NetworkGameState::InitKeysUsage()
{
	const sf::VideoMode& vm = m_gfxSettings->GetResolution();

	m_keysUsage = Button(
		Button::p2pX(69.90f, vm),
		Button::p2pY(0.90f, vm),
		Button::p2pX(30.f, vm), Button::p2pY(23.0f, vm),
		m_font, "Keys usage : \nU = Undo\nR = Restart Level\nN = Next Level\nESC = Back to Main Menu", Button::calcCharSize(vm),
		sf::Color(255, 255, 51), sf::Color(255, 255, 51), sf::Color(255, 255, 51),
		sf::Color(51, 51, 255, 250), sf::Color(51, 51, 255, 250), sf::Color(51, 51, 255, 250));
}


void NetworkGameState::Render(sf::RenderTarget& target)
{
	target.draw(m_background);

	if (m_networkMode == NetworkMode::SELECT_MODE) RenderSelectButtons();
	else if (m_networkMode == NetworkMode::SERVER)  RenderNumberButtons();
	else if (m_networkMode == NetworkMode::CLIENT)
	{
		target.draw(m_ipEntered);
		target.draw(m_enterIp);
	}
	else if (m_networkMode == NetworkMode::GAME)
	{
		m_keysUsage.Render(target);
		m_map->Render(target);
	}
}

void NetworkGameState::InitEnterIpText()
{
	m_enterIp.setFont(*m_font);
	m_enterIp.setFillColor(sf::Color::Yellow);
	m_enterIp.setPosition(Button::p2pX(12.50f, m_gfxSettings->GetResolution()),
		Button::p2pY(38.50f, m_gfxSettings->GetResolution()));
	m_enterIp.setString("Please, enter the ip/name of the server you want to connect to: ");
	m_enterIp.setCharacterSize(20);
}

void NetworkGameState::InitTextField()
{
	m_ipEntered.setFont(*m_font);
	m_ipEntered.setFillColor(sf::Color::Red);
	m_ipEntered.setCharacterSize(20);
	m_ipEntered.setPosition(Button::p2pX(40.0f, m_gfxSettings->GetResolution()),
		Button::p2pY(43.0f, m_gfxSettings->GetResolution()));
}

void NetworkGameState::InitSelectModeButtons()
{
	m_selectModeButtons["JOIN_SERVER"]= Button(
		Button::p2pX(40.0f, m_gfxSettings->GetResolution()),
		Button::p2pY(50.0f, m_gfxSettings->GetResolution()),
		Button::p2pX(25.f, m_gfxSettings->GetResolution()),
		Button::p2pY(8.0f, m_gfxSettings->GetResolution()),
		m_font, "Join Server", Button::calcCharSize(m_gfxSettings->GetResolution()),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color::Transparent, sf::Color(20, 20, 20, 0),
		sf::Color::Transparent, sf::Color::Yellow);

	m_selectModeButtons["JOIN_SERVER"].SetId(1);

	m_selectModeButtons["CREATE_SERVER"]= Button(
		Button::p2pX(40.0f, m_gfxSettings->GetResolution()),
		Button::p2pY(65.0f, m_gfxSettings->GetResolution()),
		Button::p2pX(25.0f, m_gfxSettings->GetResolution()),
		Button::p2pY(8.0f, m_gfxSettings->GetResolution()),
		m_font, "Create Server", Button::calcCharSize(m_gfxSettings->GetResolution()),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color::Transparent, sf::Color(20, 20, 20, 0),
		sf::Color::Transparent, sf::Color::Yellow);

	m_selectModeButtons["CREATE_SERVER"].SetId(2);

}

void NetworkGameState::InitServerButtons()
{
	sf::Vector2f size = { 25.f, 6.f };
	sf::Vector2f pos = { 40.f, 50.f };

	m_serverButtons["TWO_PLAYERS"] = Button(
		Button::p2pX(pos.x, m_gfxSettings->GetResolution()),
		Button::p2pY(pos.y, m_gfxSettings->GetResolution()),
		Button::p2pX(size.x, m_gfxSettings->GetResolution()),
		Button::p2pY(size.y, m_gfxSettings->GetResolution()),
		m_font, "2 Players", Button::calcCharSize(m_gfxSettings->GetResolution()),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color::Transparent, sf::Color(20, 20, 20, 0),
		sf::Color::Transparent, sf::Color::Yellow);

	pos.y += 10.f;

	m_serverButtons["TWO_PLAYERS"].SetId(m_serverButtons.size());

	m_serverButtons["THREE_PLAYERS"] = Button(
		Button::p2pX(pos.x, m_gfxSettings->GetResolution()),
		Button::p2pY(pos.y, m_gfxSettings->GetResolution()),
		Button::p2pX(size.x, m_gfxSettings->GetResolution()),
		Button::p2pY(size.y, m_gfxSettings->GetResolution()),
		m_font, "3 Players", Button::calcCharSize(m_gfxSettings->GetResolution()),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color::Transparent, sf::Color(20, 20, 20, 0),
		sf::Color::Transparent, sf::Color::Yellow);

	pos.y += 10.f;

	m_serverButtons["THREE_PLAYERS"].SetId(m_serverButtons.size());

	m_serverButtons["FOUR_PLAYERS"] = Button(
		Button::p2pX(pos.x, m_gfxSettings->GetResolution()),
		Button::p2pY(pos.y, m_gfxSettings->GetResolution()),
		Button::p2pX(size.x, m_gfxSettings->GetResolution()),
		Button::p2pY(size.y, m_gfxSettings->GetResolution()),
		m_font, "4 Players", Button::calcCharSize(m_gfxSettings->GetResolution()),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color::Transparent, sf::Color(20, 20, 20, 0),
		sf::Color::Transparent, sf::Color::Yellow);

	m_serverButtons["FOUR_PLAYERS"].SetId(m_serverButtons.size());
}

