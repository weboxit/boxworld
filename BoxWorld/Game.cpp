#include "pch.h"
#include "Game.h"

void Game::InitGraphicsSettings()
{
	m_gfxSettings = std::make_shared<GraphicsSettings>();
	m_gfxSettings->LoadFromFile("./Config/graphics_settings.ini");
}

void Game::InitWindow()
{
	if (m_gfxSettings->IsFullscreen())
			m_window=std::make_shared<sf::RenderWindow>(
			m_gfxSettings->GetResolution(),
			m_gfxSettings->GetTitle(),
			sf::Style::Fullscreen);
	else
			m_window= std::make_shared<sf::RenderWindow>(
				m_gfxSettings->GetResolution(),
				m_gfxSettings->GetTitle(),
				sf::Style::Titlebar | sf::Style::Close);

	m_window->setFramerateLimit(m_gfxSettings->GetFramRateLimit());
	m_window->setVerticalSyncEnabled(m_gfxSettings->IsVerticalSync());

}

void Game::InitKeys()
{
	m_suportedKeys = std::make_shared< std::unordered_map<std::string, uint8_t> >();

	std::ifstream fin("./Config/suported_keys.ini");

	if (fin.is_open())
	{
		std::string key_name = "";
		int key_value = 0;
		while (fin >> key_name >> key_value)
		{
			m_suportedKeys->operator[](key_name) = key_value;
		}
	}
	fin.close();
}

void Game::InitStates()
{
	m_states = std::make_shared < std::stack< State* > >();
	m_states->push(new MainMenuState(m_window, m_suportedKeys, m_gfxSettings, m_states));
}

void Game::ClearStates()
{
	while (!m_states->empty())
	{	
		delete m_states->top();
		m_states->pop();
	}
} 

Game::Game()
{ 
	InitGraphicsSettings();
	InitWindow();
	InitKeys();
	InitStates();
}

Game::~Game()
{
	ClearStates();
	std::cout << "Game dtor";
}

void Game::HandleSFMLEvents()
{
	while (m_window->pollEvent(m_event))
	{
		if (m_event.type == sf::Event::Closed)
		{
			ClearStates();
			m_window->close();
		}
		else m_states->top()->HandleSFMLEvents(m_event);
	}
}

void Game::Update()
{
	if (!m_states->empty())
	{
		m_states->top()->Update(m_dt);

		if (m_states->top()->GetQuit())
		{
			delete m_states->top();
			m_states->pop();
			if(m_states->empty()) m_window->close();
		}
	}
	else m_window->close();
}

void Game::Render()
{
	m_window->clear();

	if (!m_states->empty())
		m_states->top()->Render(*m_window);

	m_window->display();
}

void Game::Run()
{
	while (m_window->isOpen())
	{
		UpdateDt();
		Update();
		HandleSFMLEvents();
		Render();
	}
}

void Game::UpdateDt()
{
	m_dt = m_dtClock.restart().asSeconds();
}

