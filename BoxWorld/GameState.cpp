#include "pch.h"
#include "GameState.h"

GameState::GameState(std::shared_ptr < sf::RenderWindow >& window,
					 const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
					 const std::shared_ptr < GraphicsSettings >& gfxSettings,
					 std::shared_ptr < std::stack < State * > >& states,
					 const std::shared_ptr<sf::Font>& font, GameMode mode)

	: State(window, suportedKeys, gfxSettings, states),
	  m_mode(mode),
	  m_font(font)
{
	InitKeyBindings();
	InitTextures();
	InitLevelsFile();
	InitMap();  
	InitBackground();
	InitKeysUsage();

	if (m_mode == GameMode::SINGLEPLAYER) InitSolutionButton();
}

GameState::~GameState()
{
	delete m_map;
	m_levelsFin.close();

	std::cout << "GameState dtor called";
}

void GameState::UpdateButtonsHover()
{
	if (m_mode == GameMode::SINGLEPLAYER) m_solutionButton.UpdateHoverState(m_mousePosWindow);
}

void GameState::InitSolutionButton()
{

	const sf::VideoMode& vm = m_gfxSettings->GetResolution();

	sf::Vector2f size = { 30.0f, 6.f };
	sf::Vector2f pos = { 69.90f, 24.5f };

	m_solutionButton = Button(
		Button::p2pX(pos.x, vm), Button::p2pY(pos.y, vm),
		Button::p2pX(size.x, vm), Button::p2pY(size.y, vm),
		m_font, "Press for solution", Button::calcCharSize(vm),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color::Transparent, sf::Color(20, 20, 20, 0),
		sf::Color::Transparent, sf::Color::Yellow
	);

}

void GameState::InitKeyBindings()
{
	std::ifstream fin("./Config/GameState-keybindings.ini");

	if (fin.is_open())
	{
		std::string first_key = "";
		std::string second_key = "";
		while (fin >> first_key >> second_key)
		{
			if (m_suportedKeys->find(second_key) != m_suportedKeys->end())
				m_keyBindings[first_key] = m_suportedKeys->at(second_key);
		}
	}
	fin.close();
}

void GameState::InitTextures()
{
	sf::Texture* boxworld_texture = new sf::Texture;
	boxworld_texture->loadFromFile("./Resources/Textures/boxworld_texture.jpeg");
	m_textures["boxworld_texture"] = boxworld_texture;

	if (!m_backgroundTexture.loadFromFile("./Resources/Backgrounds/game_background.jpg"))
	{
		throw "ERROR::Game_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
}

void GameState::InitLevelsFile()
{
	if(m_mode==GameMode::SINGLEPLAYER) m_levelsFin = std::ifstream("./Resources/Maps/easyLevels.txt");
	else m_levelsFin = std::ifstream("./Resources/Maps/multyPlayerLevels.txt");
}

void GameState::InitMap()
{
	m_map = new Map(m_textures["boxworld_texture"]);
	m_levelsFin >> *m_map;
}

void GameState::InitBackground()
{

	m_background.setSize(
		sf::Vector2f
		(
			static_cast<float>(m_gfxSettings->GetResolution().width),
			static_cast<float>(m_gfxSettings->GetResolution().width)
		)
	);

	m_background.setTexture(&m_backgroundTexture);
}

void GameState::InitKeysUsage()
{
	const sf::VideoMode& vm = m_gfxSettings->GetResolution();

	m_keysUsage = Button(
		Button::p2pX(69.90f, vm),
		Button::p2pY(0.90f, vm),
		Button::p2pX(30.f, vm), Button::p2pY(23.0f, vm),
		m_font, "Keys usage : \nU = Undo\nR = Restart Level\nN = Next Level\nESC = Back to Main Menu", Button::calcCharSize(vm),
		sf::Color(255, 255, 51), sf::Color(255, 255, 51), sf::Color(255, 255, 51),
		sf::Color(51, 51, 255, 250), sf::Color(51, 51, 255, 250), sf::Color(51, 51, 255, 250));
}


void GameState::HandleButtons()
{
	if (m_mode == GameMode::SINGLEPLAYER)
	{
		if (m_solutionButton.IsPressed())
		{
			m_map->RestartLevel();
			m_map->Solve(m_window);
		}
	}
}


void GameState::HandleKeyboardInput(const sf::Event& current_event)
{
	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Up_Player1"]))
	{
		 m_map->Update(MutableBlock::Direction::Up, 1);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Down_Player1"]))
	{
		m_map->Update(MutableBlock::Direction::Down, 1);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Right_Player1"]))
	{
		 m_map->Update(MutableBlock::Direction::Right, 1);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Left_Player1"]))
	{
		m_map->Update(MutableBlock::Direction::Left, 1);
	}

	
	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Up_Player2"]))
	{
			m_map->Update(MutableBlock::Direction::Up, 2);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Down_Player2"]))
	{
			m_map->Update(MutableBlock::Direction::Down, 2);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Right_Player2"]))
	{
			m_map->Update(MutableBlock::Direction::Right, 2);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Move_Left_Player2"]))
	{
			m_map->Update(MutableBlock::Direction::Left, 2);
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Undo"]))
	{
		m_map->Undo();
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Restart_level"]))
	{
		m_map->RestartLevel();
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Next_level"]))
	{
		if (m_map->NextLevel(m_levelsFin) == false)
			std::cout << "No next level";
	}

	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Close"]))
	{
		SetQuit();
	}
}

bool GameState::UpdateButtonsState()
{
	if (m_mode == GameMode::SINGLEPLAYER)
	{
		if (m_solutionButton.UpdateActiveState()) return true;
		return false;
	}
	else return false;
}

void GameState::HandleSFMLEvents(const sf::Event& current_event)
{
	if (current_event.type == sf::Event::MouseButtonPressed &&
		current_event.mouseButton.button == sf::Mouse::Button::Left)
	{
		if (m_mode == GameMode::SINGLEPLAYER && UpdateButtonsState())
		{
			HandleButtons();
		}
	}

	else if (current_event.type == sf::Event::KeyPressed)
	{
		HandleKeyboardInput(current_event);
	}
}

void GameState::Update(const float& dt)
{
	UpdateMousePositions();
	m_map->UpdateLevel(m_levelsFin);
	m_map->UpdatePlayers(dt);
	UpdateButtonsHover();
}

void GameState::Render(sf::RenderTarget& target)
{
	target.draw(m_background);
	m_keysUsage.Render(target);
	m_map->Render(target);

	if (m_mode == GameMode::SINGLEPLAYER) m_solutionButton.Render(target);
}

