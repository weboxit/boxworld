#include "pch.h"
#include "MenuItem.h"

std::ostream& operator<<(std::ostream& out, const MenuItem& meniu)
{
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	
	SetConsoleTextAttribute(hStdout, meniu.m_defaultColor);
	out << meniu.m_displayText;
	SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE|FOREGROUND_GREEN|FOREGROUND_RED);
	return out;
}

MenuItem::MenuItem(std::string text, unsigned char selected, unsigned char unselected):
	m_displayText(text), m_defaultColor(selected), m_selectedColor(unselected), m_selected(false)
{
}

void MenuItem::Selected(bool selected)
{
	if (this->m_selected!=selected)
	{
		char aux;
		aux=m_selectedColor;
		m_selectedColor = m_defaultColor;
		m_defaultColor = aux;
		this->m_selected = selected;
	}
}


