#pragma once
#include "State.h"
#include "Map.h"
#include "Button.h"
#include "MultiPlayer.h"

class GameState : public State
{
public:
	enum class GameMode : uint8_t { SINGLEPLAYER, MULTIPLAYER_KEYBOARD } ;
public:
	GameState(std::shared_ptr < sf::RenderWindow >& window,
			  const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
			  const std::shared_ptr < GraphicsSettings >& gfxSettings,
		      std::shared_ptr < std::stack < State* > >& states,
			  const std::shared_ptr<sf::Font>& font, GameMode mode);
	~GameState();

public:
	void HandleSFMLEvents(const sf::Event&);
	void Update(const float& dt);
	void Render(sf::RenderTarget& target);
	void HandleKeyboardInput(const sf::Event&);
	bool UpdateButtonsState();
private:
	void UpdateButtonsHover();
	void InitSolutionButton();
	void InitKeyBindings();
	void InitTextures();
	void InitLevelsFile();
	void InitMap();
	void InitBackground();
	void InitKeysUsage();
	void HandleButtons();
private:
	GameMode m_gameMode;
	Map* m_map;
	std::ifstream m_levelsFin;
	Button m_solutionButton;
	std::shared_ptr<sf::Font> m_font;
	sf::RectangleShape m_background;
	sf::Texture m_backgroundTexture;
	Button m_keysUsage;
	GameMode m_mode;
};

