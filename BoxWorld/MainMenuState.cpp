#include "pch.h"
#include "MainMenuState.h"

MainMenuState::MainMenuState(std::shared_ptr < sf::RenderWindow >& window,
							 const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
							 const std::shared_ptr < GraphicsSettings >& gfxSettings,
							 std::shared_ptr < std::stack < State* > >& states)

	: State(window, suportedKeys, gfxSettings, states)
{
	InitKeyBindings();
	InitFont();
	InitTextures();
	InitGui();
}

MainMenuState::~MainMenuState()
{
	auto it = m_buttons.begin();
	for (it = m_buttons.begin(); it != m_buttons.end(); ++it)
	{
		delete it->second;
	}
}

void MainMenuState::Update(const float& dt)
{
	UpdateMousePositions();
	UpdateButtonsHover();
}

void MainMenuState::Render(sf::RenderTarget& target)
{
	target.draw(m_background);
	RenderButtons(target);
}

void MainMenuState::HandleKeyboardInput(const sf::Event& current_event)
{
	if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Select_Up"]))
	{
		if (m_buttonId == 0) m_buttonId++;
		else if (m_buttonId > 1) m_buttonId--;
	}

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Select_Down"]))
	{
		if (m_buttonId == 0) m_buttonId += 2;
		else if (m_buttonId >= 1 && m_buttonId < m_buttons.size()) m_buttonId++;
		else if (m_buttonId == m_buttons.size()) m_buttonId = 1;
	}

	else if (current_event.key.code == sf::Keyboard::Key(m_keyBindings["Enter_State"]))
	{
		if (UpdateActiveButtons()) 	HandleButtons();
	}

}

void MainMenuState::UpdateButtonsHover()
{
	for (auto& it : m_buttons)
	{
		it.second->UpdateHoverState(m_mousePosWindow, m_buttonId);
	}
}

void MainMenuState::InitFont()
{
	m_font = std::make_shared<sf::Font>();

	if (!m_font->loadFromFile("./Resources/Fonts/Dosis-Light.ttf"))
	{
		throw("ERROR::MAINMENUSTATE::COULD NOT LOAD FONT");
	}
}

void MainMenuState::InitKeyBindings()
{
	std::ifstream fin("./Config/main-menu_keybindings.ini");

	if (fin.is_open())
	{
		std::string first_key = "";
		std::string second_key = "";
		while (fin >> first_key >> second_key)
		{
			if (m_suportedKeys->find(second_key) != m_suportedKeys->end())
				m_keyBindings[first_key] = m_suportedKeys->at(second_key);
		}
	}
	fin.close();
}

void MainMenuState::InitTextures()
{
	sf::Texture* backgroundTexture = new sf::Texture;
	if (!backgroundTexture->loadFromFile("./Resources/Backgrounds/main-menu_background.jpg"))
	{
		throw "ERROR::MAIN_MENU_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
	else m_textures["background_texture"] = backgroundTexture;
}

bool MainMenuState::UpdateActiveButtons()
{
	for (auto& it : m_buttons)
	{
		if (it.second->UpdateActiveState()) return true;
	}
	return false;
}

void MainMenuState::HandleSFMLEvents(const sf::Event& current_event)
{
	if (current_event.type == sf::Event::MouseButtonPressed &&
		current_event.mouseButton.button == sf::Mouse::Button::Left &&
		UpdateActiveButtons())
	{
		HandleButtons();
	}

	else if (current_event.type == sf::Event::KeyPressed) HandleKeyboardInput(current_event);
}

void MainMenuState::HandleButtons()
{
	if (m_buttons["SINGLEPLAYER_STATE"]->IsPressed())
	{
		m_states->push(new GameState(m_window, m_suportedKeys, m_gfxSettings, m_states,
									m_font, GameState::GameMode::SINGLEPLAYER));
	}

	if (m_buttons["MULTIPLAYER-KEYBOARD_STATE"]->IsPressed())
	{
		std::cout << "pressed";
		m_states->push(new GameState(m_window, m_suportedKeys, m_gfxSettings, m_states,
									 m_font, GameState::GameMode::MULTIPLAYER_KEYBOARD));
	}

	if (m_buttons["MULTIPLAYER-NETWORK_STATE"]->IsPressed())
	{
		m_states->push(new NetworkGameState(m_window, m_suportedKeys, m_gfxSettings, m_states,
											m_font, m_textures["background_texture"]));
	}

	if (m_buttons["EXIT_STATE"]->IsPressed())
	{
		SetQuit();
	}

}

void MainMenuState::RenderButtons(sf::RenderTarget& target)
{
	for (auto& it : m_buttons)
	{
		it.second->Render(target);
	}
}


void MainMenuState::InitGui()
{
	const sf::VideoMode& vm = m_gfxSettings->GetResolution();

	m_background.setSize(
		sf::Vector2f
		(
			static_cast<float>(vm.width),
			static_cast<float>(vm.height)
		)
	);

	m_background.setTexture(m_textures["background_texture"]);
	
	sf::Vector2f size = { 25.f, 6.f };
	sf::Vector2f pos = { 40.f, 50.f };

	m_buttons["SINGLEPLAYER_STATE"] = new Button(
		Button::p2pX(pos.x, vm), Button::p2pY(pos.y, vm),
		Button::p2pX(size.x, vm), Button::p2pY(size.y, vm),
		m_font, "Single player", Button::calcCharSize(vm),
		sf::Color(0, 153, 153), sf::Color(255, 255, 102), sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0),
		sf::Color(255, 255, 102), sf::Color(255, 255, 102)
	);

	m_buttons["SINGLEPLAYER_STATE"]->SetId(m_buttons.size());
	pos.y += 10.f;

	m_buttons["MULTIPLAYER-KEYBOARD_STATE"] = new Button(
		Button::p2pX(pos.x, vm), Button::p2pY(pos.y, vm),
		Button::p2pX(size.x, vm), Button::p2pY(size.y, vm),
		m_font, "Multiplayer Keyboard", Button::calcCharSize(vm),
		sf::Color(0, 153, 153), sf::Color(255, 255, 102), sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0),
		sf::Color(255, 255, 102), sf::Color(255, 255, 102));

	m_buttons["MULTIPLAYER-KEYBOARD_STATE"]->SetId(m_buttons.size());

	pos.y += 10.f;

	m_buttons["MULTIPLAYER-NETWORK_STATE"] = new Button(
		Button::p2pX(pos.x, vm), Button::p2pY(pos.y, vm),
		Button::p2pX(size.x, vm), Button::p2pY(size.y, vm),
		m_font, "Multiplayer Network", Button::calcCharSize(vm),
		sf::Color(0, 153, 153), sf::Color::Green, sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0),
		sf::Color(255, 255, 102), sf::Color(255, 255, 102));

	m_buttons["MULTIPLAYER-NETWORK_STATE"]->SetId(m_buttons.size());

	pos.y += 10.f;

	m_buttons["EXIT_STATE"] = new Button(
		Button::p2pX(pos.x, vm), Button::p2pY(pos.y, vm),
		Button::p2pX(size.x, vm), Button::p2pY(size.y, vm),
		m_font, "Quit", Button::calcCharSize(vm),
		sf::Color(0, 153, 153), sf::Color(255, 255, 102), sf::Color(20, 20, 20, 50),
		sf::Color(255, 255, 102), sf::Color(150, 150, 150, 0), sf::Color(20, 20, 20, 0),
		sf::Color(255, 255, 102), sf::Color(255, 255, 102));


	m_buttons["EXIT_STATE"]->SetId(m_buttons.size());
}




