#pragma once
#include "MutableBlock.h"
#include "AnimationComponent.h"
class Player :
	public MutableBlock
{

public:
	Player(const Position& pos, sf::Texture* texture= nullptr, sf::IntRect* rect = nullptr, uint8_t col=0);
	~Player();
	void ChangeRect(sf::IntRect* rectangle);
	void Update(const float& dt);
	void Render(sf::RenderTarget& target);
	void UpdateDirection(MutableBlock::Direction);

private:
	AnimationComponent* m_ac;
	MutableBlock::Direction m_currentDirection;
	float m_resetDirectionTime;
};

