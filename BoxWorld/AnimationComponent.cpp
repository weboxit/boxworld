#include "pch.h"
#include "AnimationComponent.h"

AnimationComponent::AnimationComponent(sf::Sprite* target, int row) : m_target(target)
{
	m_row = row;
	m_totalTime = 0.0f;
	m_currentImage.y = row+1;
	m_currentImage.x = 0;
	std::srand(std::time(nullptr));
}

AnimationComponent::~AnimationComponent()
{
}

void AnimationComponent::Update(MutableBlock::Direction direction, const float& dt)
{
	float switchTime = (float)(std::rand() % 100) / 250 + 0.1f;
	m_totalTime += dt;
	if (m_totalTime >= switchTime)
	{
		m_totalTime -= switchTime;
		switch (direction)
		{
		case MutableBlock::Direction::Up:
			m_currentImage.x=4;
			if (m_currentImage.y % 2 == 0) m_currentImage.y++;
			break;
		case MutableBlock::Direction::Down:
			m_currentImage.x = 4;
			if (m_currentImage.y % 2 != 0) m_currentImage.y--;
			break;
		case MutableBlock::Direction::Left:
			m_currentImage.x = 1;
			if (m_currentImage.y % 2 == 0) m_currentImage.y++;
			break;
		case MutableBlock::Direction::Right:
			m_currentImage.x = 0;
			if (m_currentImage.y % 2 == 0) m_currentImage.y++;
			break;
		default:
			if (m_currentImage.y % 2 == 0) m_currentImage.y++;
			m_currentImage.x++;
			if (m_currentImage.x > 4)
			{
				m_currentImage.x = 0;
			}
			break;
		}
	}

	int texture_size = m_target->getTextureRect().width;
	m_target->setTextureRect(sf::IntRect(texture_size * m_currentImage.x, texture_size * m_currentImage.y, texture_size, texture_size));
}
