#pragma once
#include "MutableBlock.h"

class AnimationComponent
{
public:
	AnimationComponent(sf::Sprite* target, int row = 1);
	~AnimationComponent();
	void Update(MutableBlock::Direction direction, const float& dt);
private:
	float m_totalTime;
	int m_row;
	sf::Sprite* m_target;
	sf::Vector2u m_currentImage;
};

