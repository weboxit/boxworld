#pragma once
#include "StaticBlock.h"

class Way: public StaticBlock
{
public:
	Way(const Position& pos, sf::Texture* texture=nullptr, sf::IntRect* rect = nullptr);
	~Way();
};

