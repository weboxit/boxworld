#pragma once
#include <list>
#include "Client.h"
#include <thread>
class Server
{

public:
	void Start(int nr_of_playeri=4);
	void Stop();
	bool isRunning();
	Server();
	~Server();

private:
	void SEND(std::string ce);

private:
	SOCKET serversocket = INVALID_SOCKET;
	int m_nrOfThreads, m_nrOfPlayers;
	std::list<Client> m_clients;
	std::list<std::thread*> m_tid;
	std::thread *m_acceptthread=nullptr;
	
};

