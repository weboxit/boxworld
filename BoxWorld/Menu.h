#pragma once
#include "MenuItem.h"
class Menu
{

public:
	friend std::ostream& operator << (std::ostream& out, const Menu& menu);
	void AddMenuItems(std::string item);
	Menu();
	int Display();

private:
	std::vector <MenuItem> m_vectorItem;

};