#pragma once
class Button
{
public:
	Button();
	Button(float x, float y, float width, float height,
		const std::shared_ptr<sf::Font>& font, std::string text, unsigned character_size,
		sf::Color text_idle_color, sf::Color text_hover_color, sf::Color text_active_color,
		sf::Color idle_color, sf::Color hover_color, sf::Color active_color,
		sf::Color outline_idle_color = sf::Color::Transparent,
		sf::Color outline_hover_color = sf::Color::Transparent,
		sf::Color outline_active_color = sf::Color::Transparent,
		uint8_t id = 0);
	~Button();


	void SetId(uint8_t id);


	const bool IsPressed() const;
	const short unsigned& GetId() const;
	const std::string GetText() const;


	void UpdateHoverState(const sf::Vector2i& mousePosWindow, uint8_t id = 0);
	bool UpdateActiveState();
	void Render(sf::RenderTarget& target);

public:
	enum class ButtonState : int8_t
	{
		Idle, Hover, Active
	};

public:
	static const float p2pX(const float perc, const sf::VideoMode& vm);
	static const float p2pY(const float perc, const sf::VideoMode& vm);
	static const unsigned calcCharSize(const sf::VideoMode& vm, const unsigned modifier=60);

private:
	void InitShape(const sf::Vector2f& size, const sf::Vector2f& pos);
	void InitText(const std::string& text, unsigned character_size);
	void SetActive();
	void SetHover();
	void SetIdle();
private:
	uint8_t m_id;
	ButtonState m_buttonState;
	sf::Text m_text;
	sf::RectangleShape m_shape;
	std::shared_ptr<sf::Font> m_font;

	sf::Color m_textIdleColor;
	sf::Color m_textHoverColor;
	sf::Color m_textActiveColor;

	sf::Color m_idleColor;
	sf::Color m_hoverColor;
	sf::Color m_activeColor;

	sf::Color  m_outlineIdleColor;
	sf::Color  m_outlineHoverColor;
	sf::Color  m_outlineActiveColor;

};