#include "pch.h"
#include "Map.h"
#include "../SolutionsDLL/Solutions.h"

Map::Map(sf::Texture* map_texture) : m_texture(map_texture)
{
	InitVariables();
}

Map::Map(const Map& map)
{
	*this = map;
	this->m_p1 = new Player(map.m_p1->GetPosition());
}


Map::Map(Map&& map) noexcept
{
	this->m_bgmap = map.m_bgmap;
	this->m_fgmap = map.m_fgmap;

	if (map.m_p1)
	{
		this->m_p1 = map.m_p1;
		map.m_p1 = nullptr;
	}

	if (map.m_p2)
	{
		this->m_p2 = map.m_p1;
		map.m_p2 = nullptr;
	}

	if (map.m_p3)
	{
		this->m_p3 = map.m_p1;
		map.m_p3 = nullptr;
	}

	if (map.m_p4)
	{
		this->m_p4 = map.m_p1;
		map.m_p4 = nullptr;
	}

	this->movesStack = map.movesStack;
	this->m_nrTargetsLeft = map.m_nrTargetsLeft;

	while (!movesStack.empty()) movesStack.pop();
	map.m_bgmap.clear();
	map.m_fgmap.clear();

}


Map::~Map()
{
	ClearMap();
}

Player* Map::GetPlayer() const
{
	return m_p1;
}

const std::string& Map::GetMapName() const
{
	return m_mapName;
}

const int16_t& Map::GetNrTargetsLeft() const
{
	return m_nrTargetsLeft;
}

const std::string Map::GetFileSolution() const
{
	std::string dir = "./Solutions/" + this->m_mapName + ".txt";
	std::fstream file;
	file.open(dir);
	std::string fileSolution;
	file >> fileSolution;
	return fileSolution;
}

const int16_t Map::GetFileSolutionSize() const
{
	return GetFileSolution().size();
}

const std::vector<std::pair<int16_t, int16_t>>& Map::GetBoxes() const
{
	return this->boxes;
}

const std::vector<std::pair<int16_t, int16_t>>& Map::GetTargets() const
{
	return this->targets;
}

void Map::SetMapName(const char* name)
{
	m_mapName = name;
}

inline void Map::InitVariables()
{
	m_p1 = m_p2 = m_p3 = m_p4 = nullptr;
	m_successRead = false;
	m_nrTargetsLeft = 0;
}

std::istream& operator>>(std::istream& in, Map& map)
{
	map.m_successRead = false;
	map.m_solution = "";
	map.m_nrTargetsLeft = 0;
	map.ClearMap();
	uint16_t NrOfLine;
	char i = 0;
	int nrmaxim = 100;
	char line[257];
	size_t nrofcolum;
	if (in.eof())
	{
		map.m_successRead = true;
		return in;
	}
	do
	{
		in.getline(line, 256);
		nrofcolum=strlen(line);
		if (nrofcolum)
			nrmaxim = 100;
		else
			nrmaxim--;
		if(!nrmaxim)
		{
			map.m_successRead = true;
			return in;
		}
		Block::Position pos;
		for (int j = 0; j < nrofcolum; j++)
		{
			pos.first = j;
			pos.second = i;
			map.AddBlock(line[j], pos);
		}
		++i;
		std::string string(line);
	} while (line[0] != ';'|| in.eof());
	map.SetMapName(line + 1);
	SetConsoleTitleA(map.GetMapName().c_str());
	map.m_successRead = true;
	return in;

}

std::ostream& operator<<(std::ostream& out, const Map& map)
{
	for(const auto& static_block: map.m_bgmap)
	{
		auto draw = map.m_fgmap.find(static_block.second->GetPosition());
		if (draw != map.m_fgmap.end())
			out << *draw->second;
		else
		out << *(static_block.second);
	}


	return out;
}

void Map::ClearMap()
{
	while (!movesStack.empty()) movesStack.pop();

	for (const auto& static_block : m_bgmap)
	{
		delete static_block.second;
	}
	 
	for (const auto& mutable_block : m_fgmap)
	{
		delete mutable_block.second;
	}

	m_bgmap.clear();
	m_fgmap.clear();
	m_p1 = m_p2 = m_p3 = m_p4 = nullptr;
	Target::ResetNumberOfTargets();
}

void Map::UpdateSolution()
{
	std::string dir = "./Solutions/" + this->m_mapName + ".txt";
	std::fstream file;
	if (this->m_nrTargetsLeft == 0)
	{
		std::ofstream fileos;
		Solutions solutionLogger(fileos);
		std::string fileSolution;
		file.open(dir);
		file >> fileSolution;
		if (fileSolution.size() > m_solution.size() || fileSolution.size() == 0 )
		{
			solutionLogger.log(dir,m_solution);
			//fileos.open(dir, std::ios::out);
			//fileos << m_solution;
		}
	}
}

bool Map::Undo()
{
	if (movesStack.empty()) return 0;
	else
	{
		auto last_move = movesStack.top();
		movesStack.pop();
		Player* player = std::get<0>(last_move);
		Block::Position pos = std::get<2>(last_move);

		m_fgmap.emplace(pos, player);
		m_fgmap.erase(player->GetPosition());
		if (std::get<1>(last_move))
		{
			MutableBlock* move_block = std::get<1>(last_move).value();
			m_fgmap.emplace(player->GetPosition(), move_block);
			m_fgmap.erase(move_block->GetPosition());
			Box* isBox = dynamic_cast<Box*>(move_block);
			if (isBox != nullptr)
			{
				Target* wasOnTarget = dynamic_cast<Target*>(m_bgmap[move_block->GetPosition()]);
				if (wasOnTarget != nullptr)
				{
					isBox->ChangeState(false);
					wasOnTarget->SetBoxOver(false);
				}

				Target* isOnTarget = dynamic_cast<Target*>(m_bgmap[player->GetPosition()]);
				if (isOnTarget)
				{
					isBox->ChangeState(true);
					isOnTarget->SetBoxOver(true);
				}
			}
			move_block->SetPosition(player->GetPosition());
		}
		player->SetPosition(pos);
		return 1;
	}
}

bool Map::operator==(const Map& map2) const
{
	if (this->m_fgmap.size() == map2.m_fgmap.size() && this->m_bgmap.size() == this->m_bgmap.size() && this->m_p1->GetPosition() == map2.m_p1->GetPosition() && this->m_nrTargetsLeft == map2.m_nrTargetsLeft)
	{
		int foundOkMutable = 0;
		int foundOkStatics = 0;
		for (const auto& entry : this->m_fgmap) {
			for (const auto& entry2 : map2.m_fgmap) {
				if (entry.second->GetPosition() == entry2.second->GetPosition() && entry.second->GetSymbol() == entry2.second->GetSymbol())
				{
					foundOkMutable++;
					break;
				}
			}
		}

		if (foundOkMutable == this->m_fgmap.size()/* && foundOkStatics == this->m_bgmap.size()*/) return true;
		else return false;
	}
	else return false;
	return true;
}

Map& Map::operator=(const Map& map)
{
	this->movesStack = map.movesStack;
	this->m_nrTargetsLeft = map.m_nrTargetsLeft;
	//this->m_fgmap = map.m_fgmap;
	this->m_fgmap.clear();
	for (const auto& entry : map.m_bgmap)
	{
		AddBlock(entry.second->GetSymbol(), entry.second->GetPosition());
		if(entry.second->GetSymbol() == '*')
		targets.push_back(entry.second->GetPosition());
	}
	for (const auto& entry : map.m_fgmap)
	{
		AddBlock(entry.second->GetSymbol(), entry.second->GetPosition());
		if (entry.second->GetSymbol() == '&')
			boxes.push_back(entry.second->GetPosition());
	}
	this->m_successRead = map.m_successRead;
	this->m_nrTargetsLeft = map.m_nrTargetsLeft;
	this->m_p1 = new Player(map.m_p1->GetPosition());
	this->m_mapName = map.m_mapName;
	AddBlock(this->m_p1->GetSymbol(), this->m_p1->GetPosition());
	return *this;
}

Map& Map::operator = (Map&& map) noexcept
{
	std::cout << "move op";
	if (this == &map) return *this;

	this->ClearMap();

	this->m_bgmap = map.m_bgmap;
	this->m_fgmap = map.m_fgmap;

	if (map.m_p1)
	{
		this->m_p1 = map.m_p1;
		map.m_p1 = nullptr;
	}

	if (map.m_p2)
	{
		this->m_p2 = map.m_p1;
		map.m_p2 = nullptr;
	}

	if (map.m_p3)
	{
		this->m_p3 = map.m_p1;
		map.m_p3 = nullptr;
	}

	if (map.m_p4)
	{
		this->m_p4 = map.m_p1;
		map.m_p4 = nullptr;
	}

	this->movesStack = map.movesStack;
	this->m_nrTargetsLeft = map.m_nrTargetsLeft;

	while (!movesStack.empty()) movesStack.pop();
	map.m_bgmap.clear();
	map.m_fgmap.clear();

	return *this;
}

void Map::UpdateLevel(std::istream& in)
{
	if (Target::NoTargetsLeft()) NextLevel(in);
}

bool Map::NextLevel(std::istream& in)
{
	if (!in.eof())
	{
		in >> *this;
		return true;
	}
	else return false;
}

void Map::RestartLevel()
{
	while(Undo()!=0) {}
}

Block::Position Map::SearchPosition(MutableBlock::Direction direction, MutableBlock* move_block)
{
	Block::Position search_pos;
	switch (direction)
	{
	case MutableBlock::Direction::Up:
		search_pos.first = move_block->GetPosition().first;
		search_pos.second = move_block->GetPosition().second - 1;
		break;
	case MutableBlock::Direction::Down:
		search_pos.first = move_block->GetPosition().first;
		search_pos.second = move_block->GetPosition().second + 1;
		break;
	case MutableBlock::Direction::Left:
		search_pos.first = move_block->GetPosition().first - 1;
		search_pos.second = move_block->GetPosition().second;
		break;
	case MutableBlock::Direction::Right:
		search_pos.first = move_block->GetPosition().first + 1;
		search_pos.second = move_block->GetPosition().second;
		break;
	}
	return search_pos;
}

void Map::UpdateForeground(MutableBlock::Direction direction, std::optional<MutableBlock*> move_block)
{
	m_fgmap.erase(m_p1->GetPosition());
	m_fgmap[SearchPosition(direction, m_p1)]= m_p1;
	if (move_block)
	{ 
		m_fgmap.emplace(SearchPosition(direction, move_block.value()), move_block.value());
		move_block.value()->Move(direction);
		movesStack.emplace(std::tuple(m_p1, move_block.value(), m_p1->GetPosition()));
	}
	else movesStack.emplace(std::tuple(m_p1, std::nullopt, m_p1->GetPosition()));
	m_p1->Move(direction);
}

char transformDirectionToChar(const MutableBlock::Direction& direction) {

	switch (direction) {
	case MutableBlock::Direction::Up:
		return 'u';
	case MutableBlock::Direction::Down:
		return 'd';
	case MutableBlock::Direction::Right:
		return 'r';
	case MutableBlock::Direction::Left:
		return 'l';
	}
}

void Map::Update(MutableBlock::Direction direction)
{
	m_p1->UpdateDirection(direction);
	Block::Position pos_afterPlayer = SearchPosition(direction, m_p1);
	Block* block_afterPlayer = m_bgmap[pos_afterPlayer];
	if (dynamic_cast<Wall*>(block_afterPlayer) == nullptr)
	{
		if (m_fgmap.find(pos_afterPlayer) != m_fgmap.end())
		{
			MutableBlock* move_block = m_fgmap[pos_afterPlayer];
			Block::Position pos_after_moveBlock = SearchPosition(direction, move_block);
			if (m_fgmap.find(pos_after_moveBlock) == m_fgmap.end() &&
				 dynamic_cast<Wall*>(m_bgmap[pos_after_moveBlock])==nullptr)
			{
				Box* box = dynamic_cast<Box*>(move_block);
				if (box != nullptr)
				{
					Target* target1 = dynamic_cast<Target*>(m_bgmap[pos_afterPlayer]);
					Target* target2 = dynamic_cast<Target*>(m_bgmap[pos_after_moveBlock]);
					if (target1 != nullptr && target2 == nullptr)
					{
						target1->SetBoxOver(false);
						box->ChangeState(false);
						m_nrTargetsLeft++;
					}
					else if (target1 == nullptr && target2 != nullptr)
					{
						target2->SetBoxOver(true);
						box->ChangeState(true);
						m_nrTargetsLeft--;
					}
				}
				UpdateForeground(direction, move_block);
				m_solution.push_back(transformDirectionToChar(direction));
				UpdateSolution();
			}
			else return;
		}
		else {
			UpdateForeground(direction, std::nullopt);
			m_solution.push_back(transformDirectionToChar(direction));
			UpdateSolution();
		}
	}
	else return;
}

bool Map::CanMove(MutableBlock::Direction direction)
{
	Block::Position pos_afterPlayer = SearchPosition(direction, m_p1);
	Block* block_afterPlayer = m_bgmap[pos_afterPlayer];
	if (dynamic_cast<Wall*>(block_afterPlayer) == nullptr)
	{
		if (m_fgmap.find(pos_afterPlayer) != m_fgmap.end())
		{
			MutableBlock* move_block = m_fgmap[pos_afterPlayer];
			Block::Position pos_after_moveBlock = SearchPosition(direction, move_block);
			if (m_fgmap.find(pos_after_moveBlock) == m_fgmap.end() &&
				dynamic_cast<Wall*>(m_bgmap[pos_after_moveBlock]) == nullptr)
				return true;
			else return false;
		}
		else return true;
	}
	else return false;
}

void Map::Update(MutableBlock::Direction direction, int playernumber)
{
	Player* sec = m_p1;
	switch (playernumber)
	{
	case 2:
		if(m_p2!=nullptr)
		m_p1 = m_p2;
		break;
	case 3:
		if (m_p3 != nullptr)
		m_p1 = m_p3;
		break;
	case 4:
		if (m_p4 != nullptr)
		m_p1 = m_p4;

	}
	Update(direction);
	m_p1 = sec;
}

void Map::UpdatePlayers(const float& dt)
{
	if (this == nullptr) return;
	if (m_p1) m_p1->Update(dt);
	if (m_p2) m_p2->Update(dt);
	if (m_p3) m_p3->Update(dt);
	if (m_p4) m_p4->Update(dt);
}

void Map::AddBlock(const char character, const Block::Position& pos)
{
	if (m_texture) AddGraphicBlock(character, pos);
	else AddConsoleBlock(character, pos);
				
}

void Map::AddConsoleBlock(const char character, const Block::Position& pos)
{
	switch (character) {
	case '@': //add player si way in bg
		m_fgmap.emplace(pos, new Player(pos));
		m_bgmap.emplace(pos, new Way(pos));
		if (m_p1 == nullptr)
		{
			m_p1 = static_cast<Player*> (m_fgmap[pos]);
			m_p1->SetSymbol('1');
		}
		else if (m_p2 == nullptr)
		{
			m_p2 = static_cast<Player*> (m_fgmap[pos]);
			m_p2->SetSymbol('2');
		}
		else if (m_p3 == nullptr)
		{
			m_p3 = static_cast<Player*> (m_fgmap[pos]);
			m_p3->SetSymbol('3');
		}
		else if (m_p4 == nullptr)
		{
			m_p4 = static_cast<Player*> (m_fgmap[pos]);
			m_p4->SetSymbol('4');
		}
		break;
	case '&'://add box
		m_fgmap.emplace(pos, new Box(pos));
		m_bgmap.emplace(pos, new Way(pos));
		break;
	case '$': //add player over target
		m_fgmap.emplace(pos, new Player(pos));
		m_bgmap.emplace(pos, new Target(pos));
		if (m_p1 == nullptr)
		{
			m_p1 = static_cast<Player*> (m_fgmap[pos]);
			m_p1->SetSymbol('1');
		}
		else if (m_p2 == nullptr)
		{
			m_p2 = static_cast<Player*> (m_fgmap[pos]);
			m_p2->SetSymbol('2');
		}
		else if (m_p3 == nullptr)
		{
			m_p3 = static_cast<Player*> (m_fgmap[pos]);
			m_p3->SetSymbol('3');
		}
		else if (m_p4 == nullptr)
		{
			m_p4 = static_cast<Player*> (m_fgmap[pos]);
			m_p4->SetSymbol('4');
		}
		break;
		m_nrTargetsLeft++;
		break;
	case '+':// box over target
		m_fgmap.emplace(pos, new Box(pos));
		m_bgmap.emplace(pos, new Target(pos));
		static_cast<Target*>(m_bgmap[pos])->SetBoxOver(true);
		static_cast<Box*>(m_fgmap[pos])->ChangeState(true);
		break;
	case '-':// Way
		m_bgmap.emplace(pos, new Way(pos));
		break;
	case '#': //Wall
		m_bgmap.emplace(pos, new Wall(pos));
		break;
	case '*':// target
		m_bgmap.emplace(pos, new Target(pos));
		m_nrTargetsLeft++;
		break;
	

	}
}



void Map::AddGraphicBlock(const char character, const Block::Position& pos)
{
	sf::Vector2u texture_size = m_texture->getSize();
	texture_size.x /= 8;
	texture_size.y /= 8;

	switch (character) {
	case '@': //add player si way in bg
		m_bgmap.emplace(pos, new Way(pos, m_texture, &sf::IntRect(texture_size.x * 5, texture_size.y*0, texture_size.x, texture_size.y)));
		AddGraphicPlayer(pos, texture_size);
		Sleep(100);
		break;
	case '$': //add player over target
		m_bgmap.emplace(pos, new Target(pos, m_texture, &sf::IntRect(texture_size.x * 5, texture_size.y*1, texture_size.x, texture_size.y)));
		AddGraphicPlayer(pos, texture_size);
		Sleep(100);
		m_nrTargetsLeft++;
		break;
	case '&'://add box
		m_fgmap.emplace(pos, new Box(pos, m_texture, &sf::IntRect(texture_size.x * 7, texture_size.y*0, texture_size.x, texture_size.y)));
		m_bgmap.emplace(pos, new Way(pos, m_texture, &sf::IntRect(texture_size.x * 5, texture_size.y*0, texture_size.x, texture_size.y)));
		break;
	case '+':// box over target
		m_fgmap.emplace(pos, new Box(pos, m_texture, &sf::IntRect(texture_size.x * 7, texture_size.y*0, texture_size.x, texture_size.y)));
		m_bgmap.emplace(pos, new Target(pos, m_texture, &sf::IntRect(texture_size.x * 5, texture_size.y*1, texture_size.x, texture_size.y)));
		static_cast<Target*>(m_bgmap[{pos}])->SetBoxOver(true);
		static_cast<Box*>(m_fgmap[{pos}])->ChangeState(true);
		break;
	case '-':// Way
		m_bgmap.emplace(pos, new Way(pos, m_texture, &sf::IntRect(texture_size.x * 5, texture_size.y*0, texture_size.x, texture_size.y)));
		break;
	case '#': //Wall
		m_bgmap.emplace(pos, new Wall(pos, m_texture, &sf::IntRect(texture_size.x * 6, texture_size.y*1, texture_size.x, texture_size.y)));
		break;
	case '*':// target
		m_bgmap.emplace(pos, new Target(pos, m_texture, &sf::IntRect(texture_size.x * 5, texture_size.y*1, texture_size.x, texture_size.y)));
		m_nrTargetsLeft++;
		break;


	}
}

void Map::AddGraphicPlayer(const Block::Position& pos, const sf::Vector2u& texture_size)
{
	if (m_p1 == nullptr)
	{
		m_fgmap.emplace(pos, new Player(pos, m_texture, &sf::IntRect(texture_size.x * 4, texture_size.y * 0, texture_size.x, texture_size.y), 0));
		m_p1 = static_cast<Player*> (m_fgmap[pos]);
	}
	else if (m_p2 == nullptr)
	{
		m_fgmap.emplace(pos, new Player(pos, m_texture, &sf::IntRect(texture_size.x * 4, texture_size.y * 2, texture_size.x, texture_size.y), 2));
		m_p2 = static_cast<Player*> (m_fgmap[pos]);
	}
	else if (m_p3 == nullptr)
	{
		m_fgmap.emplace(pos, new Player(pos, m_texture, &sf::IntRect(texture_size.x * 4, texture_size.y * 4, texture_size.x, texture_size.y), 4));
		m_p3 = static_cast<Player*> (m_fgmap[pos]);
	}
	else if (m_p4 == nullptr)
	{
		m_fgmap.emplace(pos, new Player(pos, m_texture, &sf::IntRect(texture_size.x * 4, texture_size.y * 6, texture_size.x, texture_size.y), 6));
		m_p4 = static_cast<Player*> (m_fgmap[pos]);
	}
}

void Map::Render(sf::RenderTarget& target)
{
	if (m_bgmap.size()<=0)
		return;
	for (const auto& entry : m_bgmap)
	{
		if (m_successRead)
		{
			if (m_fgmap.find(entry.first) != m_fgmap.end()) m_fgmap[entry.first]->Render(target);
			else entry.second->Render(target);
		}
	}
}









void Map::ExecuteMoves(const std::string& moves, std::shared_ptr < sf::RenderWindow > window)
{
	for (char move : moves) {
		switch (move) {
		case 'u':
			this->Update(MutableBlock::Direction::Up);
			break;
		case 'd':
			this->Update(MutableBlock::Direction::Down);
			break;
		case 'l':
			this->Update(MutableBlock::Direction::Left);
			break;
		case 'r':
			this->Update(MutableBlock::Direction::Right);
			break;
		default:
			break;
		}
		if (window)
		{
			//window->clear(sf::Color::White);
			Render(*window);
			window->display();
		}
		else 
		std::cout << *this;
		Sleep(100);
	}
}





void Map::Solve(std::shared_ptr < sf::RenderWindow > window)
{
	ExecuteMoves(GetFileSolution(), window);

	std::cout << *this;
}







