#include "pch.h"
#include "Way.h"


Way::Way(const Position& pos, sf::Texture* texture, sf::IntRect* rect) : StaticBlock(pos, texture, rect)
{
	if (!texture)
	{
		SetSymbol('-');
		SetColor(10);
	}
}

Way::~Way()
{
}


