#include "pch.h"
#include "Player.h"

Player::Player(const Position& pos, sf::Texture* texture, sf::IntRect* rect, uint8_t col) : MutableBlock(pos, texture, rect)
{
	if (!texture)
	{
		SetSymbol('@');
		SetColor(FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	} 
	else
	{
		m_ac = new AnimationComponent(m_sprite, col);
	}
}

Player::~Player()
{
}

void Player::ChangeRect(sf::IntRect* rectangle)
{
	m_sprite->setTextureRect(*rectangle);
}

void Player::Update(const float& dt)
{
	m_resetDirectionTime -= dt;
	if (m_resetDirectionTime < 0)
	{
		m_currentDirection = MutableBlock::Direction::None;
		m_resetDirectionTime = 0;
	}
	m_ac->Update(m_currentDirection, dt);
}

void Player::Render(sf::RenderTarget& target)
{
}

void Player::UpdateDirection(MutableBlock::Direction dir)
{
	m_resetDirectionTime=(float)(std::rand() % 100) / 700.0f + 1.0f;
	m_currentDirection = dir;
}



