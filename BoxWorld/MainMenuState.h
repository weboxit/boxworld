#pragma once
#include "State.h"
#include "Button.h"
#include "GameState.h"
#include "NetworkGameState.h"

class MainMenuState : public State
{
public:
	MainMenuState(std::shared_ptr < sf::RenderWindow >& window,
				  const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
				  const std::shared_ptr < GraphicsSettings >& gfxSettings,
				  std::shared_ptr < std::stack < State* > >& states);
	~MainMenuState();
	void Update(const float& dt) override;
	void Render(sf::RenderTarget&) override;
	void HandleSFMLEvents(const sf::Event&) override;
private:
	void HandleKeyboardInput(const sf::Event&);
	bool UpdateActiveButtons();
	void UpdateButtonsHover();
	void InitFont();
	void InitKeyBindings() override;
	void InitTextures()  override;
	void HandleButtons();
	void RenderButtons(sf::RenderTarget& target);
	void InitGui();
private:
    sf::RectangleShape m_background;
	std::shared_ptr< sf::Font > m_font;
	std::unordered_map<std::string, Button*> m_buttons;
	uint8_t m_buttonId;
};

