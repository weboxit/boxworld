#include "pch.h"
#include "Level.h"
#include "MutableBlock.h"
#include "Target.h"

Level::Level()
{
}

Level::Level(const std::string& file)
{
	m_filename = file;
}

void Level::StartLevel()
{
	unsigned int option;
	std::cout << "Press WASD:\n";
	system("cls");
	std::ifstream f(m_filename);
	do {
		f >> m_map;
		std::cout << m_map;

		while (Target::NoTargetsLeft() == false)
		{
			option = _getch();
			switch (option)
			{
			case 0:
			{
				option = _getch();
				switch (option)
				{
				case 75:
					m_map.Update(MutableBlock::Direction::Left, 3);
					break;
				case 80:
					m_map.Update(MutableBlock::Direction::Down, 3);
					break;
				case 72:
					m_map.Update(MutableBlock::Direction::Up, 3);
					break;
				case 77:
					m_map.Update(MutableBlock::Direction::Right, 3);
					break;

				}
			}
				break;
			case 224:
			{
				option = _getch();
				switch (option)
				{
				case 75:
					m_map.Update(MutableBlock::Direction::Left,2);
					break;
				case 80 :
					m_map.Update(MutableBlock::Direction::Down,2);
					break;
				case 72:
					m_map.Update(MutableBlock::Direction::Up,2);
					break;
				case 77:
					m_map.Update(MutableBlock::Direction::Right,2);
					break;
				
				}
			
			}
			break;
			case 'w':
			case 'W':
			case VK_UP:
				m_map.Update(MutableBlock::Direction::Up);
				break;
			case 's':
			case 'S':
				m_map.Update(MutableBlock::Direction::Down);
				break;
			case 'a':
			case 'A':
				m_map.Update(MutableBlock::Direction::Left);
				break;
			case 'd':
			case 'D':
				m_map.Update(MutableBlock::Direction::Right);
				break;

			case 'n':
			case 'N':
				system("cls");
				f >> m_map;
				break;
			case 'u':
			case 'U':
				m_map.Undo();
				break;
			}
			std::cout << m_map;
		}
		std::cout << "YOU WON !!!!";
		std::cin.get();
		system("cls");
		
	} while (!f.eof());
	
}

