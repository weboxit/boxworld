#include "pch.h"
#include "Client.h"
#include <mutex>

Client::Client()
{
	WSADATA wsaData;
	WSAStartup(WINSOCK_VERSION, &wsaData);
}

bool Client::IsClose()
{
	return client == INVALID_SOCKET;
}

bool Client::Connect(std::string ip, int port)
{
	Close();
	client = socket(AF_INET, SOCK_STREAM, 0);
	addrinfo* result = NULL, hints;

	ZeroMemory(&hints, sizeof(hints));	// memset to 0
	hints.ai_family = AF_INET;			// IPv4

	

	// *** Resolve the server address and port (can be also names like "localhost") ***
	int iResult = getaddrinfo(ip.c_str(),"0", &hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo failed: %d\n", iResult);
		freeaddrinfo(result);
		WSACleanup();	// note: use WSACleanup when done working with sockets
		return 0;
	}
	sockaddr_in* ip_addr= (sockaddr_in *) (result->ai_addr);
	ip_addr->sin_port = htons(port);
	return !connect(client, result->ai_addr, (int)result->ai_addrlen);

	
}

Client::Client(SOCKET connection):client(connection)
{
}
std::mutex g_i_mutex;
void Client::Send(std::string messaje)
{
	const std::lock_guard<std::mutex> lock(g_i_mutex);
	send(client, messaje.c_str(),strlen( messaje.c_str()), 0);
}

std::string Client::Recv()
{
	char recvbuf[10240] = { 0 };
	int reciveleng;
	do {
		reciveleng = recv(client, recvbuf, 10240, 0);
		if (reciveleng == -1)
		{
			closesocket(client);
			client = INVALID_SOCKET;
			return "";
		}
	} while (reciveleng == 0);
	return std::string(recvbuf);
}

void Client::Close()
{
	if (client != INVALID_SOCKET)
		closesocket(client);
	client = INVALID_SOCKET;
}


Client::~Client()
{
	WSACleanup();
}
