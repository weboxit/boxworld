#include "pch.h"
#include "Box.h"

Box::Box(const Position& pos, sf::Texture* texture, sf::IntRect* rect) : MutableBlock(pos, texture, rect)
{
	if (!texture)
	{
		SetSymbol('&');
		SetColor(FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
	}
}

Box::~Box()
{
}

void Box::ChangeState(bool isOverTarget)
{
	if (isOverTarget)
	{
		if(m_sprite) m_sprite->setColor(sf::Color::Red);
		else SetColor(BACKGROUND_RED | BACKGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
	}
	else {
		if(m_sprite) m_sprite->setColor(sf::Color::Yellow);
		else SetColor(FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
	}
}

void Box::Update(const float& dt)
{
}

void Box::Render(sf::RenderTarget& target)
{
}
