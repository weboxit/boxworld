#include "pch.h"
#include "Target.h"

uint16_t Target::m_remainingTargets = 0;


Target::Target(const Position& pos, sf::Texture* texture, sf::IntRect* rect) : StaticBlock(pos, texture, rect)
{
	if (!texture)
	{
		SetSymbol('*');
		SetColor(12);
	}
	m_remainingTargets++;
}

bool Target::NoTargetsLeft()
{
	return (m_remainingTargets==0);
}

void Target::ResetNumberOfTargets()
{
	m_remainingTargets = 0;
}

void Target::SetBoxOver(bool over)
{
	if (over) m_remainingTargets--;
	else m_remainingTargets++;
}

uint16_t Target::getRemainingTargets()
{
	return m_remainingTargets;
}

