#pragma once
#include "State.h"
#include "Button.h"
#include "MultiPlayer.h"

class NetworkGameState : public State
{
public:
	NetworkGameState(std::shared_ptr < sf::RenderWindow >& window,
					 const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
					 const std::shared_ptr < GraphicsSettings >& gfxSettings,
					 std::shared_ptr < std::stack < State* > >& states,
					 const std::shared_ptr<sf::Font>& font, sf::Texture* menu_texture);

	~NetworkGameState();
	
	void HandleSFMLEvents(const sf::Event&) override;
	void Update(const float& dt) override;
	void Render(sf::RenderTarget& target) override;
private:
	void InitEnterIpText();
	void InitTextField();
	void InitSelectModeButtons();
	void InitServerButtons();
	void InitKeyBindings() override;
	void InitTextures() override;
	void InitMultiplayer();
	void InitBackground(sf::Texture* texture);
	void InitKeysUsage();

	void HandleKeyboardInput(const sf::Event&);
	bool UpdateActiveButtons(const sf::Event&);
	void HandleButtons();
	void RenderSelectButtons();
	void RenderNumberButtons();
	void InitLevelsFile();
	void UpdateButtonsHover();
	
private:
	MultiPlayer m_mp;
	Map* m_map;
	std::ifstream m_levelsFin;
	std::shared_ptr<sf::Font> m_font;
	enum class NetworkMode : int8_t { SELECT_MODE, CLIENT, SERVER, GAME };
	NetworkMode m_networkMode;
	sf::Text m_ipEntered;
	sf::Text m_enterIp;
	std::string m_stringInput;
    sf::RectangleShape m_background;
	uint8_t m_buttonId;
	sf::Texture* m_menuTexture;
	std::unordered_map<std::string, Button> m_selectModeButtons;
	std::unordered_map<std::string, Button> m_serverButtons;
	Button m_keysUsage;
};

