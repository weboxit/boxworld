#pragma once
#include "Block.h"
class StaticBlock: public Block
{

public:
	virtual ~StaticBlock();

protected:
	StaticBlock(const Position& pos, sf::Texture* texture, sf::IntRect* rect = nullptr);

};

