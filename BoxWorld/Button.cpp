#include "pch.h"
#include "Button.h"

Button::Button()
{
	m_buttonState = ButtonState::Idle;
	m_font = nullptr;
	m_id = 0;
}

Button::Button(float x, float y, float width, float height,
	           const std::shared_ptr<sf::Font>& font, std::string text, unsigned character_size,
			   sf::Color text_idle_color, sf::Color text_hover_color, sf::Color text_active_color,
			   sf::Color idle_color, sf::Color hover_color, sf::Color active_color,
			   sf::Color outline_idle_color, sf::Color outline_hover_color, sf::Color outline_active_color,
			   uint8_t id)
	: m_font(font),  m_textIdleColor(text_idle_color), m_textHoverColor(text_hover_color),
      m_textActiveColor(text_active_color), m_idleColor(idle_color), m_hoverColor(hover_color),
      m_activeColor(active_color), m_outlineIdleColor(outline_idle_color),
	  m_outlineHoverColor(outline_hover_color), m_outlineActiveColor(outline_active_color)
{
	m_buttonState = ButtonState::Idle;
	m_id = id;
	InitShape(sf::Vector2f(width, height), sf::Vector2f(x, y));
	InitText(text, character_size);
}

Button::~Button()
{

}

const float Button::p2pX(const float perc, const sf::VideoMode& vm)
{
	return std::floor(static_cast<float>(vm.width)* (perc / 100.f));
}

const float Button::p2pY(const float perc, const sf::VideoMode& vm)
{
	return std::floor(static_cast<float>(vm.height)* (perc / 100.f));
}

const unsigned Button::calcCharSize(const sf::VideoMode& vm, const unsigned modifier)
{
	return static_cast<unsigned>((vm.width + vm.height) / modifier);
}

void Button::InitShape(const sf::Vector2f& size, const sf::Vector2f& pos)
{
	m_shape.setPosition(pos);
	m_shape.setSize(size);
	m_shape.setFillColor(m_idleColor);
	m_shape.setOutlineThickness(1.f);
	m_shape.setOutlineColor(m_outlineIdleColor);
}

void Button::InitText(const std::string& text, unsigned character_size)
{
	m_text.setFont(*m_font);
	m_text.setString(text);
	m_text.setFillColor(m_textIdleColor);
	m_text.setCharacterSize(character_size);
	m_text.setPosition(
		m_shape.getPosition().x + (m_shape.getGlobalBounds().width / 2.f) - m_text.getGlobalBounds().width / 2.f,
		m_shape.getPosition().y 
	);
}


const bool Button::IsPressed() const
{
	if (m_buttonState == ButtonState::Active)
		return true;
	return false;
}

const std::string Button::GetText() const
{
	return m_text.getString();
}

const short unsigned& Button::GetId() const
{
	return m_id;
}

void Button::SetId(uint8_t id)
{
	m_id = id;
}

inline void Button::SetActive()
{
	m_buttonState = ButtonState::Active;

	m_shape.setFillColor(m_activeColor);
	m_text.setFillColor(m_textActiveColor);
	m_shape.setOutlineColor(m_outlineActiveColor);

}

inline void Button::SetHover()
{
	m_buttonState = ButtonState::Hover;

	m_shape.setFillColor(m_hoverColor);
	m_text.setFillColor(m_textHoverColor);
	m_shape.setOutlineColor(m_outlineHoverColor);
}

inline void Button::SetIdle()
{
	m_shape.setFillColor(m_idleColor);
	m_text.setFillColor(m_textIdleColor);
	m_shape.setOutlineColor(m_outlineIdleColor);
}

void Button::UpdateHoverState(const sf::Vector2i& mousePosWindow, uint8_t id)
{
	m_buttonState = ButtonState::Idle;

	if (m_shape.getGlobalBounds().contains(static_cast<sf::Vector2f>(mousePosWindow)) || 
		( m_id != 0 && m_id == id ) )
	{
		SetHover();
	} 
	
	if (m_buttonState == ButtonState::Idle) SetIdle();
}

bool Button::UpdateActiveState()
{
	if (m_buttonState == ButtonState::Hover)
	{
		SetActive();
		return true;
	}
	return false;
}

void Button::Render(sf::RenderTarget& target)
{
	target.draw(m_shape);
	target.draw(m_text);
}
