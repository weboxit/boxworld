#include "pch.h"
#include "State.h"

State::State(std::shared_ptr < sf::RenderWindow >& window,
			 const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
			 const std::shared_ptr < GraphicsSettings >& gfxSettings,
			 std::shared_ptr < std::stack < State* > >&  states)

	: m_window(window),
	  m_suportedKeys(suportedKeys),
	  m_gfxSettings(gfxSettings),
	  m_states(states)
{

}

State::~State()
{
	for (auto& it : m_textures) delete it.second;
	m_textures.clear();
}

void State::UpdateMousePositions()
{
	m_mousePosScreen = sf::Mouse::getPosition();
	m_mousePosWindow = sf::Mouse::getPosition(*m_window);
	m_mousePosView = m_window->mapPixelToCoords(m_mousePosWindow);
}

bool State::GetQuit() const
{
	return m_quit;
}

void State::SetQuit()
{
	m_quit = true;
}
