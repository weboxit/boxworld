#include "pch.h"
#include "AI.h"

struct Generation {
	Map map;
	//int16_t nrTargetsLeft;
	std::string moves;
	float cost=0;
	Generation(const Map& map) {
		this->map = map;
	}
	Generation(const Generation& gen) {
		this->map = gen.map;
		this->moves = gen.moves;
	}
	void setCost(const float newCost) {
		cost = newCost;
	}
	void addDirection(const char c) {
		moves.push_back(c);
	}
	template <typename T,typename V>
	float euclidianDistance(V pointStart, std::pair<T, T> pointEnd) {
		return std::sqrt(std::pow(pointEnd.first - pointStart.first, 2) + std::pow(pointEnd.second - pointStart.second, 2));
	}
	void updateCost() {
		float distMinBoxToTarget = std::numeric_limits<float>::max();
		for (int i = 0; i < this->map.GetBoxes().size(); i++)
		{
			cost += euclidianDistance(this->map.GetPlayer()->GetPosition(), this->map.GetBoxes()[i]);
			for (int j = 0; j < this->map.GetTargets().size(); j++)
			{
				float distBoxToTarget = euclidianDistance(this->map.GetBoxes()[i], this->map.GetTargets()[j]);
				if (distBoxToTarget < distMinBoxToTarget)
					distMinBoxToTarget = distBoxToTarget;
			}
		cost += distMinBoxToTarget; 
		}
	}
	void updatePlayerPos(int16_t x, int16_t y) {
		Block::Position pos(x, y);
		this->map.GetPlayer()->SetPosition(pos);
	}

};

struct MyComparator {
	bool operator() (const Generation *arg1,const Generation *arg2) {
		return arg1->cost > arg2->cost;
	}
};

AI::AI()
{

}

AI::AI(const	Map& map)
{
	m_map = map;
}

bool isVisitedAndAnalisez(const Generation& g, const std::list<Generation*> &w)
{
	for (std::list<Generation*>::const_iterator iterator = w.begin(), end = w.end(); iterator != end; ++iterator) {
		if (g.map == (*iterator)->map)
			return true;
	}

	return false;
}

bool AI::AStar()
{
	std::ofstream f("generari.txt");
	std::priority_queue<Generation*, std::vector<Generation*>,MyComparator> states;

	std::pair<int16_t, int16_t> player_box_poz;
	Generation start(m_map);
	states.push(&start);
	int iterations = 0;
	int generated = 0;
	std::list<Generation*> visitedAndAnalised;
	visitedAndAnalised.push_back(&start);

	while (!states.empty())
	{
		Generation* actual = states.top();
		states.pop();
		std::cout << actual->map;
		if (actual->map.CanMove(MutableBlock::Direction::Up))
		{
			Generation* upMap = new Generation(*actual);
			upMap->map.Update(MutableBlock::Direction::Up);
			upMap->updateCost();
			if (!isVisitedAndAnalisez(upMap->map, visitedAndAnalised) )
			{
					upMap->addDirection('u');
					states.push(upMap);
					visitedAndAnalised.push_back(upMap);
					if (upMap->map.GetNrTargetsLeft() == 0) {
						std::cout << upMap->moves;
						writeSolution(upMap->map.GetMapName(), upMap->moves);
						break;
					}
			}
			generated++;
		}
		if (actual->map.CanMove(MutableBlock::Direction::Down))
		{
			Generation* downMap = new Generation(*actual);
			downMap->map.Update(MutableBlock::Direction::Down);
			downMap->updateCost();
			if (!isVisitedAndAnalisez(downMap->map, visitedAndAnalised) )
			{
					downMap->addDirection('d');
					states.push(downMap);
					visitedAndAnalised.push_back(downMap);
					if (downMap->map.GetNrTargetsLeft() == 0) {
						std::cout << downMap->moves;
						writeSolution(downMap->map.GetMapName(), downMap->moves);
						break;
					}
			}
			generated++;
		}
		if (actual->map.CanMove(MutableBlock::Direction::Left))
		{
			Generation* leftMap = new Generation(*actual);
			leftMap->map.Update(MutableBlock::Direction::Left);
			leftMap->updateCost();
			if (!isVisitedAndAnalisez(leftMap->map, visitedAndAnalised))
			{
					leftMap->addDirection('l');
					states.push(leftMap);
					visitedAndAnalised.push_back(leftMap);
					if (leftMap->map.GetNrTargetsLeft() == 0) {
						std::cout << leftMap->moves;
						writeSolution(leftMap->map.GetMapName(), leftMap->moves);
						break;
					}
			}
			generated++;
		}
		if (actual->map.CanMove(MutableBlock::Direction::Right))
		{
			Generation* rightMap = new Generation(*actual);
			rightMap->map.Update(MutableBlock::Direction::Right);
			rightMap->updateCost();
			if (!isVisitedAndAnalisez(rightMap->map, visitedAndAnalised))
			{
					rightMap->addDirection('r');
					states.push(rightMap);
					visitedAndAnalised.push_back(rightMap);
					if (rightMap->map.GetNrTargetsLeft() == 0) {
						std::cout << rightMap->moves;
						writeSolution(rightMap->map.GetMapName(), rightMap->moves);
						break;
					}
			}
			generated++;
		}

		iterations++;
	}
	std::cout << generated;

	return false;
}


void AI::runAStartOverMaps(const std::string& fileName)
{
	std::ifstream file(fileName);
	Map map;
	file >> map;
	while (!file.eof()) {
		AI ai(map);
		ai.AStar();
		file >> map;	

	}
}

void AI::runAStartOverMaps(const std::string& fileName, const std::string& startMapName)
{
	std::ifstream file(fileName);
	Map map;
	
	file >> map;
	while (map.GetMapName() != startMapName) {
		file >> map;
	}
	if(map.GetMapName() == startMapName) {
		AI ai(map);
		ai.AStar();

	}
}

void AI::writeSolution(const std::string& mapName, const std::string& solution)
{
	std::string dir = "./Solutions/" + mapName + ".txt";
	std::ofstream file;
	Solutions logger(file);
	logger.log(dir,solution);

}


