#pragma once
#include "StaticBlock.h"

class Wall: public StaticBlock
{
public:
	Wall(const Position& pos, sf::Texture* texture=nullptr, sf::IntRect* rect = nullptr);
	~Wall();
};