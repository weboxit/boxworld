#pragma once
#include "GameState.h"
#include "MainMenuState.h"
#include "State.h"
#include "GraphicsSettings.h"

class Game
{
public:
	Game();
	virtual ~Game();
public:
	void Run();
private:
	void Render();
	void UpdateDt();
	void HandleSFMLEvents();
	void Update();
	void InitGraphicsSettings();
	void InitWindow();
	void InitKeys();
	void InitStates();
	void ClearStates();
private:
	std::shared_ptr < sf::RenderWindow > m_window;
	sf::Event m_event;
	float m_dt;
	sf::Clock m_dtClock;
	std::shared_ptr < std::stack < State* > > m_states;
	std::shared_ptr < std::unordered_map<std::string, uint8_t> > m_suportedKeys;
	std::shared_ptr < GraphicsSettings > m_gfxSettings;
};

