#include "pch.h"
#include "GraphicsSettings.h"

GraphicsSettings::GraphicsSettings()
{
	m_title = "DEFAULT";
	m_resolution = sf::VideoMode::getDesktopMode();
	m_fullscreen = false;
	m_verticalSync = false;
	m_frameRateLimit = 120;
	m_videoModes = sf::VideoMode::getFullscreenModes();
}

GraphicsSettings::~GraphicsSettings()
{
	std::cout << "Gfx dtor called";
}

void GraphicsSettings::SaveToFile(const std::string path)
{
	std::ofstream ofs(path);

	if (ofs.is_open())
	{
		ofs << m_title;
		ofs << m_resolution.width << " " << m_resolution.height;
		ofs << m_fullscreen;
		ofs << m_frameRateLimit;
		ofs << m_verticalSync;
	}

	ofs.close();
}

void GraphicsSettings::LoadFromFile(const std::string path)
{
	std::ifstream ifs(path);

	if (ifs.is_open())
	{
		std::getline(ifs, m_title);
		ifs >> m_resolution.width >> m_resolution.height;
		ifs >> m_fullscreen;
		ifs >> m_frameRateLimit;
		ifs >> m_verticalSync;
	}
}

const std::string& GraphicsSettings::GetTitle() const
{
	return m_title;
}

const sf::VideoMode& GraphicsSettings::GetResolution() const
{
	return m_resolution;
}

const bool GraphicsSettings::IsFullscreen() const
{
	return m_fullscreen;
}

const bool GraphicsSettings::IsVerticalSync() const
{
	return m_verticalSync;
}

const unsigned GraphicsSettings::GetFramRateLimit() const
{
	return m_frameRateLimit;
}

const std::vector<sf::VideoMode>& GraphicsSettings::GetVideoModes() const
{
	return m_videoModes;
}


