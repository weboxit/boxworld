#pragma once

class MenuItem
{

public:
	friend std::ostream& operator << (std::ostream& out, const MenuItem& meniu);
	MenuItem(std::string text, unsigned char selected = FOREGROUND_BLUE| FOREGROUND_INTENSITY, unsigned char unselected= BACKGROUND_BLUE| BACKGROUND_RED| BACKGROUND_GREEN|BACKGROUND_INTENSITY);
	void Selected(bool selected);

private:
	unsigned char m_defaultColor, m_selectedColor;
	bool m_selected;
	std::string m_displayText;
};

