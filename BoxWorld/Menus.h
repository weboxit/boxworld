#pragma once
#include "Menu.h"

class MainMenu
{

public:
	Menu mainMenu;
	Menu singlePlayer;
	Menu multiplayer;
	Menu multiplayerType;
	Menu multiplayerNr;
	MainMenu();
	void run();
	
};

