#pragma once

class GraphicsSettings
{
public:
	GraphicsSettings();
	~GraphicsSettings();
	void SaveToFile(const std::string path);
	void LoadFromFile(const std::string path);
	const std::string& GetTitle() const;
	const sf::VideoMode& GetResolution() const;
	const bool IsFullscreen() const;
	const bool IsVerticalSync() const;
	const unsigned GetFramRateLimit() const;
	const std::vector<sf::VideoMode>& GetVideoModes() const;
private:
	std::string m_title;
	sf::VideoMode m_resolution;
	bool m_fullscreen;
	bool m_verticalSync;
	unsigned m_frameRateLimit;
	std::vector<sf::VideoMode> m_videoModes;

};

