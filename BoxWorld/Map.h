 #pragma once
#include "Box.h"
#include "Wall.h"
#include "Way.h"
#include "Player.h"
#include "Target.h"

class Map
{
public:
	// Constructors:

	Map(sf::Texture* map_texture = nullptr);
	Map(const Map& map); //copy constructor
	Map(Map&& map) noexcept;

	//Destructor:

	~Map();

	// Getters:

	Player* GetPlayer() const;
	const std::string& GetMapName() const;
	const int16_t& GetNrTargetsLeft() const;
	const std::string GetFileSolution() const;
	const int16_t GetFileSolutionSize() const;
	const std::vector<std::pair<int16_t, int16_t>>& GetBoxes() const;
	const std::vector<std::pair<int16_t, int16_t>>& GetTargets() const;

	//Setters:

	void SetMapName(const char* name);

	//Init functions:
	void InitVariables();

	//Game logic functions:
	void Update(MutableBlock::Direction direction);
	void Update(MutableBlock::Direction direction, int playernumber);
	void UpdateLevel(std::istream& in);
	bool NextLevel(std::istream& in);
	void RestartLevel();
	bool Undo();

	//AI functions:
	void ExecuteMoves(const std::string& moves, std::shared_ptr < sf::RenderWindow > window = nullptr);
	void Solve(std::shared_ptr < sf::RenderWindow > window = nullptr);
	bool CanMove(MutableBlock::Direction direction);

	//Graphics functions:

	void Render(sf::RenderTarget& target);
	void UpdatePlayers(const float& dt);

	// operators implementation:

	friend std::istream& operator >> (std::istream& in, Map& map);
	friend std::ostream& operator << (std::ostream& out, const Map& map);
	bool operator == (const Map& other) const;
	Map& operator = (const Map& map);
	Map& operator = (Map&& map) noexcept; //move operator

private:
	//functions:

	Block::Position SearchPosition(MutableBlock::Direction direction, MutableBlock*);
	void UpdateForeground(MutableBlock::Direction, std::optional<MutableBlock*>);
	void AddBlock(const char character, const Block::Position& pos);
	void AddConsoleBlock(const char character, const Block::Position& pos);
	void AddGraphicBlock(const char character, const Block::Position& pos);
	void AddGraphicPlayer(const Block::Position& pos, const sf::Vector2u& texture_size);
	void ClearMap();
	void UpdateSolution();

private:
	//hash functor:
	struct hash_pair {
		template <class T1, class T2>
		size_t operator()(const std::pair<T1, T2>& p) const
		{
			auto hash1 = std::hash<T1>{}(p.first);
			auto hash2 = std::hash<T2>{}(p.second);
			return hash1 ^ hash2;
		}
	}; 
private:
	//variables:

	bool m_successRead;
	std::unordered_map<Block::Position, StaticBlock*, hash_pair> m_bgmap;
	std::unordered_map<Block::Position, MutableBlock*, hash_pair>m_fgmap;
	std::vector<std::pair<int16_t,int16_t>> targets; //for AI manhattan distance target-box
	std::vector<std::pair<int16_t, int16_t>> boxes;
	sf::Texture* m_texture;
	Player *m_p1,*m_p2,*m_p3,*m_p4;
	std::stack < std::tuple < Player*, std::optional<MutableBlock* >, Block::Position>> movesStack;
	int16_t m_nrTargetsLeft;
	std::string m_mapName;
	std::string m_solution;
};

