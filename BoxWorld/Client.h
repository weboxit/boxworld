#pragma once
#include <iostream>
#include <ws2tcpip.h>
#include <WinSock2.h>
#pragma comment(lib,"ws2_32")
class Client
{

public:
	Client();
	bool IsClose();
	bool Connect(std::string ip, int port);
	Client(SOCKET connection);
	void Send(std::string messaje);
	std::string Recv();
	void Close();
	~Client();

private:
	SOCKET client = INVALID_SOCKET;
};

