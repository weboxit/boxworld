#include "pch.h"
#include "MultiPlayer.h"
#include <string>
#include <sstream>

void MultiPlayer::ready()
{
	std::string recv;
	do {
		recv = m_client.Recv();
	} while (recv == "");
	m_player = recv;
	if (m_player[0] == '1')
	{
		m_player = "1";
		
	}
	while (m_client.Recv().compare("done"))
	{
		//	m_client.Send(recv);
		Sleep(100);
	}
}

void MultiPlayer::Play()
{
	m_isplayng = true;
	m_recvthread=new std::thread ([&]() {
		while (m_isplayng)
			processing(m_client.Recv());

		});
	
}

void MultiPlayer::processing(std::string messaj)
{
	while (messaj.size() > 0)
	{
		switch (messaj[0])
		{
		case 'U':
			m_map->Undo();
			break;
		case 'R':
			m_map->RestartLevel();
			break;
		case 'G':
			m_gameover = true;
			break;
		case 'L':
		{
			if (!m_isplayng)
				return;
			std::istringstream in(messaj.substr(1));
			
			in >> *m_map;
			
			system("cls");
			std::cout << *m_map;
			if (messaj.find_last_of('\n') >= 0)
			{
				messaj = messaj.substr(messaj.find_last_of('\n') + 1);
				if (messaj.size() < 2)
					return;
			}
			break;
		}
		
		case '1':
		{
			switch (messaj[1])
			{
			case 'u':
				m_map->Update(MutableBlock::Direction::Up, 1);
				break;
			case 'd':
				m_map->Update(MutableBlock::Direction::Down, 1);
				break;
			case 'l':
				m_map->Update(MutableBlock::Direction::Left, 1);
				break;
			case 'r':
				m_map->Update(MutableBlock::Direction::Right, 1);
				break;
			}

			break;
		}
		case '2':
		{
			switch (messaj[1])
			{
			case 'u':
				m_map->Update(MutableBlock::Direction::Up, 2);
				break;
			case 'd':
				m_map->Update(MutableBlock::Direction::Down, 2);
				break;
			case 'l':
				m_map->Update(MutableBlock::Direction::Left, 2);
				break;
			case 'r':
				m_map->Update(MutableBlock::Direction::Right, 2);
				break;
			}
			break;
		}
		case '3':
		{
			switch (messaj[1])
			{
			case 'u':
				m_map->Update(MutableBlock::Direction::Up, 3);
				break;
			case 'd':
				m_map->Update(MutableBlock::Direction::Down, 3);
				break;
			case 'l':
				m_map->Update(MutableBlock::Direction::Left, 3);
				break;
			case 'r':
				m_map->Update(MutableBlock::Direction::Right, 3);
				break;
			}
			break;
		}
		case '4':
		{
			switch (messaj[1])
			{
			case 'u':
				m_map->Update(MutableBlock::Direction::Up, 4);
				break;
			case 'd':
				m_map->Update(MutableBlock::Direction::Down, 4);
				break;
			case 'l':
				m_map->Update(MutableBlock::Direction::Left, 4);
				break;
			case 'r':
				m_map->Update(MutableBlock::Direction::Right, 4);
				break;
			}
			break;
		}

		}
		std::cout << *m_map;
		messaj = messaj.substr(2);
	}
}

void MultiPlayer::Levelchanger()
{ 
	m_levelchangerthread = new std::thread([&]() {
		std::ifstream in(m_file_name);
		while (!in.eof())
		{
			if (m_isplayng)
			in >> *this;
			while (Target::NoTargetsLeft())
				if (m_isplayng)
					Sleep(100);
				else
					return;
			while (!Target::NoTargetsLeft())
				if (m_isplayng)
				Sleep(100);
				else
					return;
		}
		m_client.Send("G ");
	});
}

void MultiPlayer::NextLevel()
{
}

MultiPlayer::MultiPlayer()
{
	
}

void MultiPlayer::MoveUp()
{
	std::string aux = m_player;
	m_client.Send(m_player.append("u"));
	m_player = aux;
}

void MultiPlayer::MoveDown()
{
	std::string aux = m_player;
	m_client.Send(m_player.append("d"));
	m_player = aux;
}
void MultiPlayer::MoveLeft()
{
	std::string aux = m_player;
	m_client.Send(m_player.append("l"));
	m_player = aux;
}

void MultiPlayer::MoveRight()
{
	std::string aux = m_player;
	m_client.Send(m_player.append("r"));
	m_player = aux;
}

bool MultiPlayer::Connect(std::string ipaddress, int port)
{
	if (!m_client.Connect(ipaddress, port))
	{
		std::cout << "Disconnected";
		return false;
	}
		std::cout << "Connected";
	ready();
	std::cout << "Ready";
	Play();
	
	return true;
}

void MultiPlayer::Server(int nr_of_player)
{
	m_server.Start(nr_of_player);
	std::cout << "Server started";
	Sleep(2000);
	if (!m_client.Connect("127.0.0.1", 9876))
	{
		std::cout << "Disconnected";
		return;
	}
	std::cout << "Connected";
	ready();
	std::cout << "Ready";
	Levelchanger();
	Play();
}

void MultiPlayer::Setmap(Map* map)
{
	m_map = map;
}

void MultiPlayer::SetFile(std::string filename)
{
	m_file_name= filename;

}

void MultiPlayer::Undo()
{
	m_client.Send("U ");
}

void MultiPlayer::RestartLevel()
{
	m_client.Send("R ");
}

void MultiPlayer::ConsolePlay()
{
	
	
	do {

		while (Target::NoTargetsLeft() == false)
		{
			int option = _getch();
			switch (option)
			{
			case 0:
			{
				option = _getch();
				switch (option)
				{
				case 75:
					MoveLeft();
					break;
				case 80:
					MoveDown();
					break;
				case 72:
					MoveUp();
					break;
				case 77:
					MoveRight();
					break;

				}
			}
			break;
			case 224:
			{
				option = _getch();
				switch (option)
				{
				case 75:
					MoveLeft();
					break;
				case 80:
					MoveDown();
					break;
				case 72:
					MoveUp();
					break;
				case 77:
					MoveRight();
					break;

				}

			}
			break;
			case 'w':
			case 'W':
			case VK_UP:
				MoveUp();
				break;
			case 's':
			case 'S':
				MoveDown();
				break;
			case 'a':
			case 'A':
				MoveLeft();
				break;
			case 'd':
			case 'D':
				MoveRight();
				break;
			case 'u':
			case 'U':
				Undo();
				break;
			case 'r':
			case 'R':
				RestartLevel();
				break;



			}


		}


		} while (!m_gameover);

	}


MultiPlayer::~MultiPlayer()
{
	m_isplayng = false;
	if (m_recvthread != nullptr)
		if (m_recvthread->joinable())
			m_recvthread->join();
	delete m_recvthread;
	if (m_levelchangerthread != nullptr)
		if (m_levelchangerthread->joinable())
			m_levelchangerthread->join();
	delete m_levelchangerthread;
	m_client.Close();
	
	m_server.Stop();
}

std::istream& operator>>(std::istream& in, MultiPlayer& mp)
{

	std::string out = "L";
	int leng = 0;
	char line[1024] = { 0 };
	do
	{
		for (int i = 0; i < leng; i++)
			line[i] = 0;
		in.getline(line, 1023);
		line[strlen(line)] = '\n';
		line[strlen(line)] = 0;
		out.append(line);
		leng = strlen(line);
		Sleep(1);
	} while (!in.eof() && out.find(";", 0)==std::string::npos);

	
		mp.m_client.Send(out);
	return in;
}
