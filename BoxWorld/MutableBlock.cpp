#include "pch.h"
#include "MutableBlock.h"


void MutableBlock::Update(const float& dt)
{
}

void MutableBlock::Move(Direction direction)
{
	switch (direction)
	{
	case Direction::Up:
	{
		m_position.second--;
		break;
	}
	case Direction::Down:
	{
		m_position.second++;
		break;
	}
	case Direction::Left:
	{
		m_position.first--;
		break;
	}
	case Direction::Right:
	{
		m_position.first++;
		break;
	}
	default:
		break;
	}

	if(m_sprite) SetSpritePosition(m_position);
}

MutableBlock::~MutableBlock()
{
}


MutableBlock::MutableBlock(const Position& pos, sf::Texture* texture, sf::IntRect* rect) : Block(pos, texture, rect)
{

}

