#include "pch.h"
#include "Server.h"
#include "Client.h"

void Server::SEND(std::string ce)
{
	for (Client &cl : m_clients)
		cl.Send(ce);
}

void Server::Start(int nr_of_player)
{
	m_nrOfThreads = 1;
	m_nrOfPlayers = nr_of_player;
	serversocket = socket(AF_INET, SOCK_STREAM, 0);
	sockaddr_in address;
	address.sin_addr.s_addr = ADDR_ANY;
	address.sin_family = AF_INET;
	address.sin_port =htons (9876);
	
	bind(serversocket, (sockaddr*)&address, sizeof(address));
	listen(serversocket, 10);
	m_acceptthread=new std::thread ([&]() {
		
		m_nrOfThreads++;
		
		
		while (m_clients.size() < m_nrOfPlayers && m_nrOfThreads > 0)

		{
			SOCKET acept = accept(serversocket, NULL, NULL);
			if (acept == INVALID_SOCKET)
			{
				printf("accept failed: %d\n", WSAGetLastError());
				//closesocket(ListenSocket);
				//WSACleanup();
				//nrofthreads = -5;
				
			}

			m_clients.emplace_back(acept);
			char x[4];
			_itoa_s(m_nrOfPlayers -m_clients.size()+1, x, 10);
			m_clients.back().Send(std::string(x));
			Sleep(2000);
			 
			if(m_clients.size()== m_nrOfPlayers)
			{SEND("done");
			}
			m_tid.push_back(new std::thread ([&]() {
				m_nrOfThreads++;
				Client &x = m_clients.back();
				while (m_nrOfThreads > 0)
				{
					std::string s = x.Recv();
					SEND(s);
					Sleep(1);
				}


			}));
			
		}
		while (!m_tid.empty())
		{
			m_tid.back()->join();
			delete m_tid.back();
			m_tid.pop_back();
		}
	});
	
}

void Server::Stop()
{
	m_nrOfThreads = -1;
	if(m_acceptthread != nullptr)
	if (m_acceptthread->joinable())
		m_acceptthread->join();
	m_acceptthread = nullptr;

}

bool Server::isRunning()
{
	return (m_acceptthread!=nullptr);
}

Server::Server()
{
	WSADATA wsaData;
	WSAStartup(WINSOCK_VERSION, &wsaData);
}


Server::~Server()
{
	Stop();
	
	WSACleanup();
}
