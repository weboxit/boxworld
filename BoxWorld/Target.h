#pragma once
#include "StaticBlock.h"

class Target: public StaticBlock
{

public:
	Target(const Position& pos, sf::Texture* texture=nullptr, sf::IntRect* rect = nullptr);
	static bool NoTargetsLeft();
	static void ResetNumberOfTargets();
	void SetBoxOver(bool over);
	static uint16_t getRemainingTargets();

private:
	static uint16_t m_remainingTargets;

};
