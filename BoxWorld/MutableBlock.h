#pragma once
#include"Block.h"

class MutableBlock :
	public Block
{

public:
	enum class Direction : uint8_t
	{
		Up, Down, Right, Left, None

	};
	virtual void Update(const float& dt);
	void Move(Direction);
	virtual ~MutableBlock();

protected:
	MutableBlock(const Position& pos, sf::Texture* texture= nullptr, sf::IntRect* rect = nullptr);

};

