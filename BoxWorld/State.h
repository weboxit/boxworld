#pragma once
#include "GraphicsSettings.h"

class State
{
public:
	State(std::shared_ptr < sf::RenderWindow >& window,
		  const std::shared_ptr < std::unordered_map<std::string, uint8_t> >& suportedKeys,
		  const std::shared_ptr < GraphicsSettings >& gfxSettings,
		  std::shared_ptr < std::stack < State* > >& states);
	virtual ~State();
public:
	virtual void HandleSFMLEvents(const sf::Event&) = 0;
	virtual void Update(const float& dt) = 0;
	virtual void Render(sf::RenderTarget& target) = 0;
	bool GetQuit() const;
protected:
	void UpdateMousePositions();
	virtual void InitKeyBindings() = 0;
	virtual void InitTextures() = 0;
	void SetQuit();
protected:
	std::unordered_map<std::string, sf::Texture*> m_textures;
	std::shared_ptr < sf::RenderWindow > m_window;
	std::shared_ptr < std::stack < State* > > m_states;
	std::shared_ptr < std::unordered_map<std::string, uint8_t> > m_suportedKeys;
	std::shared_ptr < GraphicsSettings > m_gfxSettings;
	std::unordered_map<std::string, uint8_t> m_keyBindings;
	sf::Vector2i m_mousePosScreen;
	sf::Vector2i m_mousePosWindow;
	sf::Vector2f m_mousePosView;
	bool m_quit;
};

