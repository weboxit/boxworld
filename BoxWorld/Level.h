#pragma once
#include "Map.h"

class Level
{

public:
	Level();
	Level(const std::string& file);
	void StartLevel();
private:
	Map m_map;
	std::vector<Player> m_players;
	bool m_done;
	std::string m_filename;
};

