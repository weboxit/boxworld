#include "pch.h"
#include "Wall.h"


Wall::Wall(const Position& pos, sf::Texture* texture, sf::IntRect* rect) : StaticBlock(pos, texture, rect)
{
	if (!texture)
	{
		SetSymbol('#');
		SetColor(11);
	}
}

Wall::~Wall()
{
}


