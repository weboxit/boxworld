#pragma once
#include "Server.h"
#include "Map.h"
#include <thread>

class MultiPlayer
{
private:
	Client m_client;
	Server m_server;
	Map* m_map;
	bool m_isplayng;
	std::string m_player, m_file_name="";
	void ready();
	void Play();
	std::thread *m_recvthread,*m_levelchangerthread;
	void processing(std::string messaj);
	void Levelchanger();
	bool m_gameover;
	friend std::istream& operator>>(std::istream&, MultiPlayer& mp);
	

public:
	void NextLevel();
	MultiPlayer();
	void Undo();
	void RestartLevel();
	void MoveUp();
	void MoveDown();
	void MoveLeft();
	void MoveRight();
	bool Connect(std::string ipaddress, int port);
	void Server(int nr_of_player=4);
	void Setmap(Map* map);
	void SetFile(std::string filename);
	
	void ConsolePlay();
	~MultiPlayer();
};

