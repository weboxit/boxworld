#pragma once
#include "Map.h"
#include "../SolutionsDLL/Solutions.h"

class AI
{

public:
	AI(const Map& map);
	AI();
	bool AStar();
	void runAStartOverMaps(const std::string& fileName);
	void runAStartOverMaps(const std::string& fileName, const std::string& startMapName);

private:
	Map m_map;

private:
	void writeSolution(const std::string& mapName, const std::string& solution);

};

