#pragma once

class Block
{
public:
	using Position = std::pair<int, int>;

public:
	virtual ~Block();
	char GetSymbol() const;
	char GetColor() const;
	Position GetPosition() const;
	void SetSymbol(const char symbol);
	void SetColor(const unsigned char color);
	void SetPosition(const Position& otherPos);
	friend std::ostream&operator<< (std::ostream&in, const Block& block);

	//SFML
	void Render(sf::RenderTarget& target);
	virtual void Update(const float& dt);
	void SetSpritePosition(Position);

private:
	void InitSprite(const Position& pos, sf::Texture* texture, sf::IntRect);

protected:
	sf::Sprite* m_sprite;
	Position m_position;
	Block(const Position& pos, sf::Texture* texture=nullptr, sf::IntRect* rect=nullptr);
	unsigned char m_color;
	char m_symbol;
};

