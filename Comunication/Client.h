#pragma once
#include <WinSock2.h>

#include <winsock2.h>


// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT "27015"
class Client
{

private:
	WSADATA wsaData;
	SOCKET ConnectSocket;
	struct addrinfo *result ,
		*ptr,
		hints;
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

public:
	Client();
	
	~Client();
};

