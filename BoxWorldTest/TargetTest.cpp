#include "pch.h"
#include "CppUnitTest.h"
#include "../BoxWorld/Target.h"
#include "../BoxWorld/Map.h"
#include<fstream>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoxWorldTest
{
	TEST_CLASS(MapTest)
	{
	public:

		TEST_METHOD(ResetNumberOfTargets)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Target::ResetNumberOfTargets();
			Assert::AreEqual((uint32_t)0, (uint32_t)Target::getRemainingTargets());

		}

		TEST_METHOD(NoTargetsLeft)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Target::ResetNumberOfTargets();
			Assert::IsFalse(Target::NoTargetsLeft());

		}
	};
}
