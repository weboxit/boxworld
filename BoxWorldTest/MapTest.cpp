#include "pch.h"
#include "CppUnitTest.h"
#include "../BoxWorld/Map.h"
#include<fstream>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoxWorldTest
{
	TEST_CLASS(MapTest)
	{
	public:

		TEST_METHOD(EqualityOperator)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Map m2;
			m2 = m1;
			bool difference = false;
			if (m1 == m2)
				difference = true;
			Assert::IsTrue(difference);

		}

		TEST_METHOD(Undo)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Map m2;
			m2 = m1;
			bool difference = false;
			if (m1 == m2)
				difference = true;
			Assert::IsTrue(difference);
			//To be ..

		}

		TEST_METHOD(AssignmentOperator)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Map m2;
			m2 = m1;
			bool difference = false;
			if (m1 == m2)
				difference = true;
			Assert::IsTrue(difference);
			//To be ..

		}

		TEST_METHOD(CopyingConstructor)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Map m2;
			m2 = m1;
			bool difference = false;
			if (m1 == m2)
				difference = true;
			Assert::IsTrue(difference);
			//To be ..

		}

		TEST_METHOD(Update)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Map m2;
			m2 = m1;
			bool difference = false;
			if (m1 == m2)
				difference = true;
			Assert::IsTrue(difference);
			//To be ..

		}

		TEST_METHOD(CanMove)
		{
			std::ifstream f("simpleMap.txt");
			Map m1;
			f >> m1;
			Map m2;
			m2 = m1;
			bool difference = false;
			if (m1 == m2)
				difference = true;
			Assert::IsTrue(difference);
			//To be ..

		}


	};
}
