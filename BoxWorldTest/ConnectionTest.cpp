#include "pch.h"
//#include "ConnectionTest.h"
#include "CppUnitTest.h"
#include "../BoxWorld/Server.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BoxWorldTest
{
	TEST_CLASS(ConnectionTest)
	{
	public: TEST_METHOD(Stat)
	{
		Client c;
		Server s;
		s.Start();
		c.Connect("127.0.0.1", 9876);
		c.Send("A");
		Assert::IsTrue(c.Recv() == "A");

	}
	};
}
