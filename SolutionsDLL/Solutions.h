#pragma once

#pragma once
#ifdef SOLUTIONS_EXPORT
#define SOLUTIONS_API __declspec(dllexport)
#else
#define SOLUTIONS_API __declspec(dllexport)
#endif

#include<iostream>
#include<fstream>
#include <string>
class SOLUTIONS_API Solutions
{
public:
	Solutions(std::ofstream& out);

	template <class F, class T>
	void log(const F& fileName, const T& input) {
		m_os.open(fileName, std::ios::out);
		m_os << input;
	}

private:
	std::ofstream& m_os;

};

