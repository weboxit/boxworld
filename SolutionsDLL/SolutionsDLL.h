// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the LOGINDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// LOGINDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SOLUTIONSDLL_EXPORTS
#define SOLUTIONSDLL_API __declspec(dllexport)
#else
#define SOLUTIONSDLL_API __declspec(dllimport)
#endif

// This class is exported from the dll
class SOLUTIONSDLL_API CSolutionsDll {
public:
	CSolutionsDll(void);
	// TODO: add your methods here.
};

extern SOLUTIONSDLL_API int nSolutionsDll;

SOLUTIONSDLL_API int fnSolutionsDll(void);
